/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
*/
#ifndef database_h
#define database_h

#define	DB_MODE	    0666

#ifndef NULL
#define NULL    0
#endif

#define    Create	(1)
#define    Update	(1 << 1)
#define    Lookup	(1 << 2)
typedef unsigned long t_db_operation;

typedef union {
    t_value    *value;
    char       *string;
}           u_record_data;

#define EMPTY		1
#define NON_EMPTY	0

extern t_db_operation db_operation;
extern void db_store_form();
extern void db_forms_init();
extern void db_forms_close();
extern t_boolean db_forms_lookup();
extern t_boolean db_forms_lookup_tbl();
extern void db_forms_complete();
extern void db_forms_dump();

#endif	/* database_h */
