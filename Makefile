# Generated automatically from Makefile.in by configure.
#
#    mmorph, MULTEXT morphology tool
#    Version 2.3, October 1995
#    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
#    Dominique Petitpierre, <petitp@divsun.unige.ch>

# use with
#       make target
# where target is one of:
#       all             build everything (default)
#       install         build and install everything from a source distribution
#       realclean       remove every reconstructible file
#       test            test the compiled tool against some saved results
#
############################################################################
# Macros most likely to be subject to customization

# directory where to install this package
MTHOME=/usr/local/pck/multext

# manual section for user commands
USERSECT=1

# manual section for file formats
FILESECT=5

# directory containing lib/libdb.a and include/db.h
# /usr/local defaults to /usr/local, but this can be changed with the
# option --prefix of configure (cf. 00INSTALL.configure)
USRLOCALDIR=/usr/local

# you probably don't need to change anything below this line
#==========================================================================

# directory in user's PATH
BINDIR=$(MTHOME)/bin
# directory in user's MANPATH
MANDIR=$(MTHOME)/man
INSTALLBIN=cp
INSTALLMAN=cp
# directory containing libdb.a
DBLIBDIR=$(USRLOCALDIR)/lib
# directory containing db.h
DBINCLUDEDIR=$(USRLOCALDIR)/include

# need flex, lex does not handle 8 bit character codes
LEX=flex
# optimize what you can
LFLAGS=-8 -Ca

YACC=bison -y

INCLUDEDIRS=-I$(DBINCLUDEDIR)
# librairies where to find system things:
# on Solaris: -lnsl for xdr functions
LIBS=
LDLIBS=  $(LIBS) -L$(DBLIBDIR) -ldb

# gcc defaults to gcc if it is available, cc otherwise.
# It can be changed by setting the environment variable CC prior to
# running ./configure  (cf. 00INSTALL.configure)
CC=gcc
# On IBM's AIX, -O breaks the program: For the moment avoid with
# CFLAGS=
# On Silicon Graphics IRIX, strange interference with db.h (symbol UINT_MAX).
# For the moment avoid with -cckr:
# CFLAGS=-cckr
# On Alpha: to allow enough room for optimizing
# CFLAGS=-O1000
# CFLAGS=-pg -g -DDEBUG
CFLAGS=-O -g

INCLUDES= config.h user.h symbols.h unify.h bitmap.h chain.h mymalloc.h \
	 combine.h database.h spell.h output.h tfs.h crc32file.h \
	 tbl.h version.h
CSOURCES=symbols.c unify.c bitmap.c chain.c output.c mymalloc.c \
	 combine.c main.c database.c lookup.c spell.c tfs.c \
	 crc32file.c tbl.c
OBJS=main.o tokenize.o y.tab.o symbols.o unify.o bitmap.o chain.o \
     output.o mymalloc.o combine.o database.o lookup.o spell.o \
     tfs.o crc32file.o tbl.o

# evaluate make commands in the Bourne shell
SHELL=/bin/sh

# MAKE=make macro definition


PROGRAM=mmorph

all: all_tools

all_tools: $(PROGRAM)

tokenize.c: tokenize.l
	rm -f $@
	$(LEX) $(LFLAGS) -t $< >$@

y.tab.h  y.tab.c: user.y
	rm -f y.tab.h y.tab.c
	$(YACC) -dvt user.y 2>&1 \
	| sed -f yacc.sed

tokenstr.h: y.tab.h  tokenstr.sed
	rm -f $@
	sed -f tokenstr.sed -n y.tab.h >$@

y.tab.o: y.tab.c $(INCLUDES) y.tab.h parse.h 
tokenize.o: tokenize.c $(INCLUDES) y.tab.h parse.h
main.o: main.c $(INCLUDES)  y.tab.h parse.h tokenstr.h
symbols.o: symbols.c $(INCLUDES)
unify.o: unify.c $(INCLUDES)
bitmap.o: bitmap.c bitmap.h mymalloc.h config.h
chain.o: chain.c chain.h mymalloc.h config.h
output.o: output.c output.h parse.h config.h
mymalloc.o: mymalloc.c mymalloc.h config.h
combine.o: combine.c $(INCLUDES)
lookup.o: lookup.c $(INCLUDES)
tfs.o: tfs.c $(INCLUDES)
tbl.o: tbl.c $(INCLUDES)
crc32file.o: crc32file.c
database.o: database.c $(INCLUDES)
	 $(CC) $(CFLAGS) $(CPPFLAGS) -c $(INCLUDEDIRS) $<
spell.o: spell.c $(INCLUDES)


$(PROGRAM): $(OBJS)
	$(CC) $(CFLAGS)  $(CPPFLAGS) -o $@ $(OBJS) $(LDLIBS)

clean:
	rm -f *.o tokenize.c y.tab.h y.tab.c tokenstr.h y.output \
	      *.CKP */*.CKP .emacs_[0-9]* *.dvi *.txt *.log \
	      example.out example.errs data/example.db data/example.tfs \
	      *.aux latex2text*.tex gmon.out mon.out \
	      config.log configure.scan config.cache

distclean: realclean
realclean: clean
	rm -f  $(PROGRAM) tags test *.BAK *.bak tokenize config.status \
		stamp-h.in stamp-h */*.BAK */*.bak

check: test
test: test1 test2
test1: $(PROGRAM)
	LC_CTYPE=C ./$(PROGRAM) -n -m data/example -l example.out
	@if { diff data/example.test example.out; } ;\
	then echo "test1 passed OK! ;-)" ;\
	else echo "test1 failed! :-(" ;\
	     exit 1 ;\
	fi

test2: $(PROGRAM)
	rm -f data/example.db data/example.tfs
	LC_CTYPE=C ./$(PROGRAM) -c -m data/example \
		    -l example.errs data/example.words example.out
	@if { diff data/example.lookup example.out; } ;\
	then echo "test2 passed OK! ;-)" ;\
	else echo "test2 failed! :-(" ;\
	     exit 1 ;\
	fi

install:  install_tools install_man

install_tools: install_bin


install_bin: $(PROGRAM) $(BINDIR)
	rm -f $(BINDIR)/$(PROGRAM)
	$(INSTALLBIN) $(PROGRAM) $(BINDIR)/$(PROGRAM)

install_man: install_man1 install_man5

install_man1: mmorph.1 $(MANDIR)/man$(USERSECT)
	rm -f $(MANDIR)/man$(USERSECT)/mmorph.$(USERSECT)
	$(INSTALLMAN) mmorph.1 $(MANDIR)/man$(USERSECT)/mmorph.$(USERSECT)

install_man5: mmorph.5 $(MANDIR)/man$(FILESECT)
	rm -f $(MANDIR)/man$(FILESECT)/mmorph.$(FILESECT)
	$(INSTALLMAN) mmorph.5 $(MANDIR)/man$(FILESECT)/mmorph.$(FILESECT)

$(MANDIR)/man$(USERSECT): $(MANDIR)
	./mkdirhier  $@

$(MANDIR)/man$(FILESECT): $(MANDIR)
	./mkdirhier $@

# Do not create MANDIR if it does not exist: the man pages would be
# probably be inaccessible anyway.
$(MANDIR):
	@if [ ! -d  $@ ] ; then \
	 echo "Manual hierarchy $@ does not exist, please create it." ;\
	 echo "Alternatively: adjust macro MANDIR in Makefile.in to be one" ;\
	 echo "of the existing paths in your MANPATH environment variable" ;\
	 echo 'then run "./configure" and "make install" again.' ;\
	 exit 1 ;\
	fi

# Do not create BINDIR  if it does not exist: the programs would be
# probably be inaccessible anyway.
$(BINDIR):
	@if [ ! -d  $@ ] ; then \
	 echo "Binary location $@ does not exist, please create it." ;\
	 echo "Alternatively: adjust macro BINDIR in Makefile.in to be one" ;\
	 echo "of the existing paths in your PATH environment variable" ;\
	 echo 'then run "./configure" and "make install" again.' ;\
	 exit 1 ;\
	fi

#================================================================
# The following are ad'hoc targets used by the developper.
# Adapt and use at your own risk


# force the use of gcc. same as "make -k  CC=gcc CFLAGS=<gcc flags>"
GCC=gcc
GCCFLAGS=-g -O -ansi -Wall
gcc:
	$(MAKE) -k CC="$(GCC)" CFLAGS="$(GCCFLAGS)" LDLIBS="$(LDLIBS)" \
		INCLUDEDIRS="$(INCLUDEDIRS)" MTHOME=$(MTHOME) \
		USERSECT=$(USERSECT) FILESECT=$(FILESECT) \
		USRLOCALDIR=$(USRLOCALDIR)
malloc:
	$(MAKE) -k  CFLAGS=-g \
	   CPPFLAGS="-I/usr/local/include" \
	   DBLIBDIR="/usr/local/lib -lmalloc"

malloc2:
	$(MAKE) -k  CFLAGS=-g \
	   CPPFLAGS="-DMALLOC_FUNC_CHECK -I/usr/local/include" \
	   DBLIBDIR="/usr/local/lib -lmalloc"

data/example.test: $(PROGRAM) data/example
	LC_CTYPE=C ./$(PROGRAM) -n -m data/example -l $@

data/example.words: data/example.test
	LC_CTYPE=iso_8859_1 ./$(PROGRAM) -n -m data/example 2>&1 \
	    | cut -d\" -f2 |sort -u >$@

data/example.lookup: $(PROGRAM) data/example.words
	rm -f data/example.db data/example.tfs
	LC_CTYPE=C ./$(PROGRAM) -c -m data/example data/example.words $@

DISTFILES= tokenize.l user.y $(CSOURCES) $(INCLUDES) parse.h Makefile \
	   yacc.sed tokenstr.sed lint.sed \
	   00INSTALL 00INSTALL.configure 00README 00CHANGES 00COPYRIGHT \
	   00RELEASE_NOTES mmorph.1 mmorph.5 mmorph_v2.tex mkdirhier \
	   data/example data/example.test data/example.lookup \
	   data/example.words data/french data/english \
	   data/lex.exp.A data/lex.exp.N data/lex.exp.V data/lex.exp.minor \
	   data/lex.proper data/german util/swap util/factorize \
	   configure configure.in Makefile.in config.h.in

mkf:
	make -f Makefile.mkf Makefile

Coda_server: FORCE
	echo '{' >tmp
	echo $(DISTFILES) \
	| tr ' ' '\012' \
	| sed -e '/^Makefile$$/d' -e '/^config.h$$/d' \
	      -e 's/^/"/' -e 's/$$/"/' >>tmp
	echo '}' >>tmp
	sed -e '/{/r tmp' -e '/{/,/}/d'  Coda_server >Coda_server.new
	mv Coda_server.new Coda_server
	rm -rf tmp

FORCE:


tokenize: tokenize.c
	$(CC) -DTEST -o tokenize tokenize.c

indent:
	for f in $(CSOURCES) $(INCLUDES) parse.h ;\
	    do if [ -f $$f ] ;\
	       then indent $$f ;\
		    mv $$f.BAK $$f.bak ;\
	       fi ; \
	done

tags: indent
	ctags -t tokenize.l user.y $(CSOURCES) $(INCLUDES) parse.h

REMOTE=issun9
backup: realclean
	(ls -d alpha* mmorph-* *.ps data/*.db data/*.tfs data/*.forms \
	       data/*.out data/*.out.gz \
	       data/lex* data2 bug >/tmp/exclude.$$ ;\
	 gtar --block-compress --totals -cvzf $(REMOTE):/dev/fd0 -b 36 \
	      . $$CVSROOT/mmorph -X /tmp/exclude.$$ ;\
	 rm -f /tmp/exclude.$$)
	make tags # prepare for new work phase (indent,tags and .BAK files)
	cp user.y user.y.bak
	cp tokenize.l tokenize.l.bak

backup2: 
	(ls -d alpha* mmorph-* *.ps data/*.db data/*.tfs data/*.forms \
	       data/*.out data/*.out.gz \
	       data/lex* data2 bug *.BAK *.bak tags >/tmp/exclude.$$ ;\
	 gtar --block-compress --totals -cvzf $(REMOTE):/dev/fd0 -b 36 \
	      . $$CVSROOT/mmorph -X /tmp/exclude.$$ ;\
	 rm -f /tmp/exclude.$$)

restore:
	mkdir backup
	(cd backup ;\
	GZIP="-q" gtar --block-compress -xzf $(REMOTE):/dev/rfd0 -b 36)

lint: 	y.tab.c  y.tab.h tokenstr.h tokenize.c
	lint $(INCLUDEDIRS) $(CSOURCES) y.tab.c tokenize.c \
	 | sed -f lint.sed

cvstag:
	cvs tag multext_2_3_4 $(DISTFILES)

solaris:
	rdist -d 'FILES=( ${DISTFILES} )' solaris

aix:
	rdist -d 'FILES=( ${DISTFILES} )' aix

alpha:
	rdist -d 'FILES=( ${DISTFILES} )' alpha

irix:
	rdist -d 'FILES=( ${DISTFILES} )' irix

sun3:
	rdist -d 'FILES=( ${DISTFILES} )' sun3

archive:
	tar cvf - ${DISTFILES} | gzip >mmorph-2.0.tar.gz

run: $(PROGRAM)
	$(PROGRAM) -d1 -n -m example

MMORPHVERSION=2
DELIVERABLEID=D2_3_1B

postscript: mmorph.1.ps mmorph.5.ps mmorph_v$(MMORPHVERSION).ps

text: mmorph.1.txt mmorph.5.txt mmorph_v$(MMORPHVERSION).txt

PSROFF=groff -Tps

mmorph.1.ps: mmorph.1
	$(PSROFF) -man mmorph.1 >mmorph.1.ps

mmorph.5.ps: mmorph.5
	$(PSROFF) -man mmorph.5 >mmorph.5.ps

mmorph_v$(MMORPHVERSION).ps:  mmorph_v$(MMORPHVERSION).tex
	latex mmorph_v$(MMORPHVERSION)
	latex mmorph_v$(MMORPHVERSION)
	dvitps mmorph_v$(MMORPHVERSION) >mmorph_v$(MMORPHVERSION).ps

mmorph.1.txt: mmorph.1
	nroff -Tlpr -man mmorph.1 | col -b >mmorph.1.txt

mmorph.5.txt: mmorph.5
	nroff -Tlpr -man mmorph.5 | col -b >mmorph.5.txt

mmorph_v$(MMORPHVERSION).txt:  mmorph_v$(MMORPHVERSION).tex
	latex2text mmorph_v$(MMORPHVERSION) > mmorph_v$(MMORPHVERSION).txt

packdoc: mmorph.1 mmorph.1.txt mmorph.1.ps mmorph.5 mmorph.5.txt \
	    mmorph.5.ps mmorph_v$(MMORPHVERSION).tex \
	    mmorph_v$(MMORPHVERSION).txt mmorph_v$(MMORPHVERSION).ps \
	    README.$(DELIVERABLEID)
	mkdir $(DELIVERABLEID)
	ln mmorph_v$(MMORPHVERSION).tex mmorph_v$(MMORPHVERSION).txt \
	    mmorph_v$(MMORPHVERSION).ps \
	    mmorph.1 mmorph.1.txt mmorph.1.ps \
	    mmorph.5 mmorph.5.txt mmorph.5.ps \
	    $(DELIVERABLEID)
	ln README.$(DELIVERABLEID) $(DELIVERABLEID)/README
	tar cvf - $(DELIVERABLEID) | gzip > $(DELIVERABLEID).tar.gz
	rm -rf  $(DELIVERABLEID)

