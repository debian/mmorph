%
%    mmorph, MULTEXT morphology tool
%    Version 2.3, October 1995
%    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
%    Dominique Petitpierre, <petitp@divsun.unige.ch>
%    Graham Russell, <russell@divsun.unige.ch>
%
\documentstyle[11pt,a4wide]{article}

% for exdented paragraphs in the bibliography - bibtex is too much
% trouble and not everybody has tib:
\def\xp{\hangafter=1\hangindent=1.5em\noindent}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{MMORPH - The Multext Morphology Program\\[0.7ex]
Version 2.3: October 1995\\[2.5ex]
Deliverable 2.3.1}

\author{Dominique Petitpierre and Graham Russell\\
ISSCO, 54 route des Acacias, CH-1227 Carouge, Switzerland\\
\verb|{petitp,russell}@divsun.unige.ch|}

\date{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Background}

As the Technical Annex points out, many Multext applications will require the
ability to perform various kinds of analysis on word tokens.  For example, in
some cases it will be necessary to abstract away from inflectional variation,
so that e.g.\ {\em walk}, {\em walks}, {\em walking}, and {\em walked\/} are
all treated as the same word type at the level of textual annotations.
Conversely, it will sometimes be desirable to make use of richer information
than that available in the raw text, so that e.g.\ {\em walking\/} can be
identified as the present participle of `walk'.  In addition, it is easy to
envisage a need for flexibility in the triangular relation between word-token,
textual annotations and lexical information; a single fixed linguistic
analysis cannot fulfil the requirements of diverse text processing tasks.
{\sf Mmorph} provides the means by which lexicons can be constructed and
modified, and texts annotated with lexical information.

Very generally, the program operates by relating the form of a word as
found in text to an entry in a lexical database containing arbitrary
information expressed in terms of attributes and values (see
section~\ref{msd-sec} below).  Various modes of interaction with {\sf mmorph}
exist, depending on whether the user is developing, compiling, or exploiting a
description (section~\ref{use-sec}).  The lexical database is created from a
set of initial lexical entries (section~\ref{lex-ent-sec}) and a set of
structural rules (sections~\ref{m-rules-sec}, \ref{sp-rules-sec}).  

For full details on the use of {\sf mmorph}, we refer the reader to the manual
pages included in the distribution; `mmorph(1)' describes command-line options
controlling the behavior of the program and `mmorph(5)' defines the format of
files containing a linguistic description.
%
% Actually, we could put them in as appendices
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Morphosyntactic Descriptions}
\label{msd-sec}

A {\bf morphosyntactic description} (MSD) is an expression in a formal
language representing arbitrary kinds of information.  Lexical entries relate
word forms to MSDs, and structural rules analyse complex words by reference to
the MSDs of their subparts.  Essentially, a MSD has two components; a `type'
and a set of attributes and values.  The type can be thought of as a gross
classification of lexical properties: `noun', `article', etc.  Finer
distinctions may be made by adding one or more attribute-value pairs to the
type; familiar examples would be attributes such as `gender' and `tense', and
values such as `neuter' and `future'.  Taken together, a neuter noun in the
accusative case might be represented by the MSD:
%
\begin{quote}\begin{verbatim}
noun[gender=neuter case=accusative]
\end{verbatim}\end{quote}
%
If we do not know or do not care whether this word is in fact nominative or
accusative, a disjunctive value may be given, as shown below:
%
\begin{quote}\begin{verbatim}
noun[gender=neuter case=accusative|nominative]
\end{verbatim}\end{quote}
%
The choice of type, attributes and values is open to the user.
We shall describe other aspects of MSD syntax when we present the
structural rules in section~\ref{m-rules-sec}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Lexicon}

\subsection{Lexical Entry Syntax}
\label{lex-ent-sec}

The initial lexical entries are of two kinds.  In the case of a regular
or unanalysable word such as {\em want\/} or {\em between}, they consist of
a simple pairing of a MSD and a string representing the basic form of the
word in question:
%
\begin{quote}\begin{verbatim}
verb[form=base]         "want"
prep[type=time|place]   "between"
\end{verbatim}\end{quote}
%

In the case of suppletive or irregular forms, it is possible to specify the
basic form of the word explicitly:
%
\begin{quote}\begin{verbatim}
noun[num=pl]        "mice"     = "mouse"
verb[form=past_prt] "gone"     = "go"
\end{verbatim}\end{quote}
%
This basic form will then be the one displayed when a word is looked up in
{\sf mmorph}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Morphological Grammar}
\label{m-rules-sec}
\subsection{Declarations}

It is necessary to declare the attributes that are to be used in the 
grammar, together with their possible values:
%
\begin{quote}\begin{verbatim}
@ Attributes
number : singular plural
person : first second third
etc.
\end{verbatim}\end{quote}
%
The association of attributes to types is also declared:
%
\begin{quote}\begin{verbatim}
@ Types
noun : gender case
verb : tense mood
etc.
\end{verbatim}\end{quote}
%
These declarations permit consistency checking over descriptions.  They also
permit the internal representations created by {\sf mmorph} to be more
efficient.

\subsection{Rules}

Morphological rules have the form exemplified below:
%
\begin{quote}\begin{verbatim}
lhs_type[att1=val1 att2=val2]
    <- rhs_type1[att3=val3 att4=val4]
       rhs_type2[att5=val5]
\end{verbatim}\end{quote}
%
The interpretation of such a rule is that a valid word-form of type
\verb|lhs_type| consists of the concatenation of the forms associated 
with \verb|rhs_type1| and \verb|rhs_type2|, providing that the three MSDs
involved have the specified values for their respective attributes.  MSDs
are interpreted by a restricted variety of graph unification (see e.g.\
Shieber 1986), in which all values are atomic.  Briefly, this has the
following consequences:
%
\begin{enumerate}

\item
The order in which attributes are written is not significant;

\item
Attributes which are not relevant to a particular rule may be omitted;

\item
Variables may be used to enforce sharing of values between different MSDs
within a single rule.

\end{enumerate}
%
For example, the following rule has the effect of creating a plural noun
from a singular noun and a plural noun suffix, ensuring that the resulting
plural has the same gender as the singular:
%
\begin{quote}\begin{verbatim}
noun[number=plural gender=$gen]
    <- noun[number=singular gender=$gen]
       noun_suffix[number=plural]
\end{verbatim}\end{quote}
%


\subsection{Affixes}

Affixes are building blocks used by the binary rules described above. They 
have the form exemplified below:
%
\begin{quote}\begin{verbatim}
"affix_string" type[att1=val1 att2=val2]
\end{verbatim}\end{quote}
%
The interpretation of such a rule is that if the specified MSD unifies with
one part of the right hand side of a binary rule and another building block
(lexical entry or the result of applying another rule) MSD unifies with the
other part then the affix string is concatenated to the other string to
form the result string, associated to the result MSDs.
%
For example, the following affix rule provides the suffix {\tt "s"} which may
concatenate with a noun to make a plural noun:
%
\begin{quote}\begin{verbatim}
"s" noun_suffix[number=plural]
\end{verbatim}\end{quote}
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Spelling Rules}
\label{sp-rules-sec}

\subsection{Lexical and Surface Forms}
\label{lex-surf-sec}

The structural rules described in section~\ref{m-rules-sec} have the effect
of concatenating word-segments.  Often, however, a complex word-form differs
from the result of concatenating its components.  Well-known examples from
English include the doubling of some final consonant characters before a
suffix-initial vowel ({\em big+er = bigger}), insertion of an epenthetic `e'
({\em box+s = boxes}) in some places, and deletion of an `e' in others ({\em
fine+est = finest}).  {\sf Mmorph} incorporates a mechanism allowing
adjustment rules to be written for these cases and many others.

It is convenient to think of such adjustments as discrepancies between two
strings of characters, one representing the `surface form' of a word, as found
in texts, and the other representing the `lexical form' composed of substrings
entered into the linguistic description.  To make this a little clearer, we
show the surface and possible lexical forms of the words mentioned above:
%
\begin{quote}\begin{verbatim}
   b i g g e r      b o x e s      f i n   e s t
   b i g   e r      b o x   s      f i n e e s t
\end{verbatim}\end{quote}
%

The spelling rules are implemented in a version of what has come to be known
as a `two-level' formalism.  As its name suggests, this permits
correspondences to be established between the surface and lexical `levels'.
Discussions of two-level rules may be found in Pulman and Hepple (1993),
Ritchie et al.\ (1992), and Ruessink (1989).


\subsection{Declarations}

As before, {\sf mmorph} requires some declarations.  The first of these
concerns character classes, e.g.:
%
\begin{quote}\begin{verbatim}
@ Classes
Vowel: a e i o u
SXZ:   s x z
\end{verbatim}\end{quote}
%
Names of classes declared in this way may be used within spelling rules in
order to generalize over the members of the class.

A second declaration concerns pairs of items from the lexical and surface
strings.  Again, sets of pairs declared in this way may be referred to by
their name within spelling rules:
%
\begin{quote}\begin{verbatim}
@ Pairs
Inserted_E:    e/<>
Double_Cons:   <b b>/b  <d d>/d  <g g>/g ...
\end{verbatim}\end{quote}
%
This example illustrates the notation for character sequences; {\tt <>}
matches against the empty string, while {\tt <b b>} matches against two
successive {\tt b}\,s.


\subsection{Two-Level Rules}

By default, each character is permitted to match itself.  Thus no rule need
be written stating that a lexical{\tt a} corresponds to a surface {\tt a},
and so on.  

The rules themselves take the form
%
\begin{quote}\begin{verbatim}
Rulename : 
    operator   LeftContext - Focus - RightContext
\end{verbatim}\end{quote}
%
The `operator' is one of `{\tt =>}', `{\tt <=}' or `{\tt <=>}', depending on
whether the rule is to license a lexical-surface correspondence, block all
non-matching correspondences, or both.  The `focus' is the active portion of
the rule; it is here that the discrepancies mentioned earlier are described.
The two contexts restrict the application.  As an example, consider a
simplified version of the rule responsible for `inserting' {\tt e} in {\em
boxes\/}:
%
\begin{quote}\begin{verbatim}
Simple_Add_E : 
    <=>   SXZ - Inserted_E - * s
\end{verbatim}\end{quote}
%
The left context consists of the character class name {\tt SXZ}, defined
above.  It is interpreted as the set of pairs {\tt \{s/s}, {\tt x/x}, {\tt
z/z\}}.  The right context consists simply of {\tt s}, similarly interpreted
as {\tt s/s}; preceding this is an asterisk identifying the boundary between
two word-segments.  \verb|Inserted_E| is the mnemonic name of the pair {\tt
e/<>}.  The rule as a whole states that whenever a lexical string contains
the sequence matching
%
\begin{quote}\begin{verbatim}
x  'boundary'  s
\end{verbatim}\end{quote}
%
the surface string must match
%
\begin{quote}\begin{verbatim}
x  e  'boundary'  s
\end{verbatim}\end{quote}
%
The rule can be restricted to apply only in the context of specific
kinds of affixes. These constraints are specified by a list of MSDs
associated to the spelling rule.
%
\begin{quote}\begin{verbatim}
Simple_Add_E : 
    <=>   SXZ - Inserted_E - * s
		noun_suffix[number=plural] verb_suffix[]
\end{verbatim}\end{quote}
%
The morpheme boundary indicated by the star determines an affix whose type
and features should be subsumed by at least one of the MSDs associated to
the spelling rule.  In the example above it would apply only in the context
of a plural noun suffix or a verb suffix.  If there is no star in the
spelling rule, then the affix is the one adjacent (and lowest in the
structure tree) to the word-segment where the first letter of the focus
occurs.  If there is no affix, then the type and features of the lexical
stem is used.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Using {\sf mmorph}}
\label{use-sec}

\subsection{Organization of a Description}

A description consists of eight declaration sections (see the manual page
mmorph(5) for details).  They can appear in just one file, or be split across
many files and referenced in the main file by \verb|#include "file"|
directives.  This is convenient for example to be able to try different
lexical entries or test different affixes with a particular description:  one
has just to change the file reference in a directive.  For extra convenience,
an option ({\tt -a}) is provided to specify on the command line a file whose
content will be used in place of the \verb|@ Lexicon| section of the main
file.  This used mainly to augment a database with new entries, or test a
restricted set of lexical entries.

\subsection{Testing}

To test the rule applications on the lexical entries specified in a
morphological description, without creating a database and without looking
up words, use the command line options \verb|-n -m morphfile -l logfile|.
The entries that would be entered in the database are written into the file
\verb|logfile|.  If this file is not specified the output goes to the
screen (standard error).

In order to do the same operations as above, but on a set of
alternate lexical entries in in a file \verb|addfile|, use the extra option
\verb|-a addfile|.  The lexical entries in morphfile will be ignored.

\subsection{Compiling}

When a set of rules and associated lexical entries is completed, a full form
database can be created with the option \verb|-c|.  If the option \verb|-a
addfile| is also specified, both the lexical entries of the main file and
of \verb|addfile| are used to create the database.  If neither the option
\verb|-c| nor \verb|-n| is specified, the database is augmented with
entries in \verb|addfile| (note that new entries are added to existing ones,
rather than replacing them).
After processing all the entries, the program reads its input (standard
input or the input file if specified) to look up words (see below).

\subsection{Accessing}

Once a full form database has been created, entries can be looked up with
%
\begin{quote}\begin{verbatim}
mmorph -m morphfile -r rejectfile inputfile outputfile
\end{verbatim}\end{quote}
%
The inputfile should consist of words, one per line to be looked up.  The
full forms and associated MSDs are written in the output file, one per
line, with a new line separating groups of full forms corresponding to the
same input word.  Words that are not found in the database are written in
the file \verb|rejectfile|.  If not specified these three files correspond
respectively to the UNIX standard input, standard output and standard
error, which allows the user to work interactively or with a piped stream.

If the input file is in the MULTEXT record/field format, word forms can be
annoted with
%
\begin{quote}\begin{verbatim}
mmorph -m morphfile -U -C class1,class2,classN inputfile outputfile
\end{verbatim}\end{quote}
%
where the option \verb|-C| specifies the list of token classes assigned by
the segmenter that should be looked up.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Restrictions and Future Extensions}
\label{restrict-sec}

This implementation has a few restrictions:
%
\begin{itemize}

\item
Different attributes declared with the the same set of values cannot be
identified by the same variable. This is not very useful with
flat feature structures anyway.

\item
No SGML input and output format is provided yet (only simple lookup and
record/field lookup).
\end{itemize}
%
Future developments are foreseen which will allow {\sf mmorph} to:
%
\begin{itemize}

\item
Make it possible to feed {\sf mmorph} with SGML marked text and
obtain the result of the lookup in an augmented SGML format. (Needs
an SGML API).

\item
Specify rules of word compounding; compounds could be analysed (recognized) 
at lookup time, rather than being stored in a full form database.

\item
Make hypotheses about unknown words at lookup time (e.g.\ `{\em xyzable\/} is 
an adjective').

\item
Segment a text into words and punctuations.  It would use the same principle
as the spelling rules applying at morpheme boundaries, but at word
boundaries.

\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{References}

\xp
Pulman, S.G. and M.R. Hepple (1993) ``A Feature-Based Formalism for Two-Level 
Phonology: A Description and Implementation''. {\em Computer Speech and 
Language\/} 7, 333--358.

\xp
Ritchie, G.D., G.J. Russell, A.W. Black, and S.G. Pulman (1992)
{\em Computational Morphology: Practical Mechanisms for the English Lexicon}
Cambridge, MA: The MIT Press

\xp
Ruessink, H. (1989) ``Two-Level Formalisms''.  Working Papers in Natural 
Language Processing no.\,5, Rijksuniversiteit Utrecht.

\xp
Shieber, S.M. (1986) {\em An Introduction to Unification-Based Approaches 
to Grammar}, Stanford: CSLI.

\newpage

\section*{Appendix A: Sample Description}
\addtolength{\baselineskip}{-0.35ex}
\begin{verbatim}
;; MULTEXT MORPHOLOGY TOOL
;; Sample treatment of English inflection
;;---------------------------------------------------------------
;; Declare the surface and lexical alphabets.  
@ Alphabets
lexical : a b c d e f g h i j k l m n o p q r s t u v w x y z 
          u_e qu
surface : a b c d e f g h i j k l m n o p q r s t u v w x y z
;;---------------------------------------------------------------
;; The attributes we'll be using, together with their values.
@ Attributes

;; NUMber: SinGular and PLural
num  : sg pl

;; PERson: 1st, 2nd, 3rd
per  : 1 2 3
    
;; VFM (Verb ForM) values:
;; bse  - "base" (read, eat, walk, etc.)
;; prp  - "present participle" (reading, eating, walking, etc.)
;; psp  - "past participle" (read, ate, walked, etc.)
;; pres - "present finite" (read, reads, eat, eats, etc.)
;; past - "past finite" (read, ate, walked, etc.)
vfm  : pres past bse psp prp

;; DEGree of adjectives: BaSE, COMParative, and SUPerlative
deg  : bse comp sup

;; INFLectable (i.e. complete word)?
infl : yes no

;; Classification of (ir)regular verbs and nouns:                              
;; r   regular: past tense in -(e)d - past participle in -(e)d
;; i1  regular past tense, irregular past participle
;; i2  irregular past tense and past participle
;; i3  dual form past tense and past participle
;; i   irregular nouns (i.e. non-standard plurals)
reg  : r i1 i2 i3 i
;;---------------------------------------------------------------
;; The types we'll be using, together with their attributes.
@ Types

noun:      num | infl
adj:       deg | infl
verb:      vfm num per | infl reg 
nsuf:      num
adjsuf:    deg 
vsuf:      num per vfm | reg

;;---------------------------------------------------------------
;; Word-structure rules.
@ Grammar

;; Goal rules define what counts as a good complete word:
;; 
GoalN:    noun[]
GoalA:    adj[]
GoalV:    verb[]

;; Plural nouns.  The RHS noun feature structure will not
;; unify with lexical entries marked as 'infl-no', so nouns
;; with irregular or non-existent plurals are not accepted.
;;
NPL:   noun[num=pl] 
       <- noun[num=sg infl=yes] 
          nsuf[num=pl]

;; affixes for this rule:
;;
N.plural:  "s" nsuf[num=pl]

;; Comparative and superlative adjectives.  The value of 'deg' 
;; in the LHS and the suffix is unified  - and restricted to 
;; 'comp' or 'sup'
;; 
ADJ1:  adj[deg=$deg=comp|sup] 
       <- adj[deg=bse infl=yes] 
          adjsuf[deg=$deg]

ADJ.comparative:   "er"   adjsuf[deg=comp]
ADJ.superlative:   "est"  adjsuf[deg=sup]

;; Various verb forms.  Again we restrict application to verbs
;; not marked as 'infl=no'.
;;
VB1:   verb[vfm=pres num=sg per=3]
       <- verb[infl=yes vfm=bse]
          vsuf[vfm=pres]

VB2:   verb[vfm=$vfm]
       <- verb[infl=yes vfm=bse reg=$reg]
          vsuf[vfm=$vfm!=pres reg=$reg]

;; affixes
VB.pres:       "s"   vsuf[vfm=pres]
VB.prp:        "ing" vsuf[vfm=prp]
VB.past.reg:   "ed"  vsuf[vfm=psp|past reg=r]
VB.past.i1:    "ed"  vsuf[vfm=past reg=i1]

;;===============================================================
;; Define character classes and give them names.
@ Classes
        
C:      b c d f g h j k l m n p q r s t v w x z
SC:     s c

;;---------------------------------------------------------------
;; Define names for sets of pairs of characters.  
;;   "X/Y" matches X in surface string and Y in lexical string.
;;   "<X Y>/Z" matches surface string XY and lexical string Y.
;;   "<>" matches the empty string.
@ Pairs
            
l1_s2_SZ:   <s s e>/s <z z e>/z

l1_s2:      <b b>/b <d d>/d <g g>/g <k k>/k <l l>/l <m m>/m 
            <n n>/n <p p>/p <r r>/r <t t>/t <v v>/v

SXZ:        s/s x/x z/z 

;; We can "inherit" from other pair sets, provided that they have 
;; already been defined (i.e. before the compiler sees this):
I_YSXZ:     i/y SXZ

EI:         e/e i/i

V_no_u_e:   a/a e/e i/i o/o u/u 

;; The general "V" pair set includes the lexical "u_e" symbol,
;; representing an "unstressed e".  See rules lex_1_surf_2[ab].
V:          e/u_e V_no_u_e 

;; Consonant pairs augmented with a match for <q u>/qu.  Lexical
;; "qu" behaves like a consonant and not a consonant-vowel 
;; sequence for the purpose of rules lex_1_surf_2[ab].
CC:         b/b c/c d/d f/f g/g h/h j/j k/k l/l m/m n/n p/p q/q 
            r/r s/s t/t v/v w/w x/x z/z ?/qu

;;---------------------------------------------------------------
;; The spelling rules.  
@ Spelling

lex_1_surf_2a:
;; big+er->bigger, hop+ing->hopping, tap+ed->tapped, etc.
;; Preceding vowel pair must not be e/u_e - see lexical entries
;; for "offer" and "prefer"
        <=>  CC V_no_u_e - l1_s2 - * V

lex_1_surf_2b:
;; bus+s->busses, fez+s->fezzes, quiz+s->quizzes, etc.
        <=>  CC V_no_u_e - l1_s2_SZ  - * s/s

lex_UE_surf_E:
;; The lexical "u_e" symbol matches surface "e" everywhere,
;; so the left and right contexts are empty.
        <=>  - e/u_e - 

lex_QU_surf_Q_U:
;; The lexical "qu" symbol matches surface "q u" everywhere.
        <=>  - <q u>/qu -

surfonly_E_1:
;; boss+s->bosses, box+s->boxes
        <=>  C SXZ * - e/<> - s/s

surfonly_E_2:
;; dish+s->dishes, catch->catches
        <=>  SC h/h * - e/<> - s/s

surfonly_E_3:
;; potato+s->potatoes
;; This gets tango+s->tangos wrong - we need to distinguish
;; the "tango-o" from the "potato-o" somehow.  There are 
;; various ways of doing this, but this description is just
;; an illustration, so we won't bother.
        <=>  C o/o * - e/<> - s

lexonly_E:
;; large+est->largest, agree+ed->agreed
        <=>  - <>/e - * EI

lex_Y_surf_I:
;; easy+er->easier, carry+s->carries, 
        <=>  C - i/y - * e/e

lex_Y_surf_IE:
;; fry+s->fries
        <=>  C - <i e>/y - * s/s

lex_I_surf_Y:
;; lie+ing->lying, etc.
        <=>  - y/i <>/e - * i/i
;;---------------------------------------------------------------
;; Now the lexicon.  This is just for testing - in real life, 
;; we would use "#include" to load a bigger list of entries.
@ Lexicon

;;
;; nouns
;;
noun[num=sg]
    "dog"
    "box"
    "boss"
;; &qu; is a special lexical symbol allowing "qu" to be treated
;; as a consonant:
    "&qu;iz" = "quiz"
    "fez"
    "dish"
    "church"
    "potato"
    "crisis"
    "crises"
;;
;; adjectives
;;
adj[deg=bse]
    "big"
    "fine"
    "waxy"

adj[deg=bse infl=no]
    "intelligent"
;;
;; verbs
;;
verb[vfm=bse reg=r]
    "walk"
    "hop"
    "fry"
    "lie"
    "race"
    "prefer"
;; &u_e; is an "unstressed e" - compare "prefer":
    "off&u_e;r"  = "offer"

verb[vfm=bse reg=i1]
    "mow"

verb[vfm=psp]
    "mown"

verb[vfm=bse reg=i2]
    "sing"

verb[vfm=past]
    "sang"

verb[vfm=psp]
    "sung"
;;---------------------------------------------------------------

\end{verbatim}


\end{document}
