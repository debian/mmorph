#!/bin/sed -f
1i\
#define FIRSTTOKEN  257\
static char *token[] = {
s/[	 ][	 ]*/ /g
/^# *define /b token
b end
: token
s/^# *define \([^ ][^ ]*\) \([0-9][0-9]*\) *$/\1/
h
s/.*/    "&",/p
: end
$i\
\    ""\
\    };
$g
$s/.*/#define LASTTOKEN &/p
