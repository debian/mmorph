/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
*/
#ifndef bitmap_h
#define bitmap_h

#include "config.h"
#include <values.h>

#define WORD_TYPE	unsigned long
#define WORD_SIZE	BITS(WORD_TYPE)
#define MODULO_MASK	(WORD_SIZE-1)
/* DIV_SHIFT is log2(WORD_SIZE) */
#if SIZEOF_LONG == 4
#define DIV_SHIFT	5
#endif
#if SIZEOF_LONG == 8
#define DIV_SHIFT	6
#endif

#define FIRST_BIT	((WORD_TYPE) 1L)
#define NO_BITS		((WORD_TYPE) 0L)
#define ALL_BITS	(~ NO_BITS)
#define WORD_OFFSET(n)	((WORD_TYPE) (n) >> DIV_SHIFT)
#define BIT_N(n)	(FIRST_BIT << ((int) (n) & MODULO_MASK))
#define SET_BIT(word,bit)	((word) |= BIT_N(bit))
#define UNSET_BIT(word,bit)	((word) &= ~ BIT_N(bit))
#define TEST_BIT(word,bit)    ((word) & BIT_N(bit))
#define FULL_SET(n) (ALL_BITS >> (WORD_SIZE - (int)(n)))

#define FOR_EACH_BIT(map,n) \
    for ((n) = next_bit((map), 0L); (n) >= 0L;\
         (n) = next_bit((map), (long) ((n) + 1L)))

typedef struct {
    long        size;
    WORD_TYPE  *bits;
}           s_bitmap;

extern long next_bit();
extern void unset_bit();
extern void set_bit();
extern int  test_bit();
extern s_bitmap *new_bitmap();
extern void free_bitmap();
extern int  has_unique_bit();
extern void assign_and();
extern void assign_or();
extern void negate_bitmap();
extern void empty_bitmap();
extern void fill_bitmap();
extern int  set_if_not_set();
extern int  test_and();

#endif	/* bitmap_h */
