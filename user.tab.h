typedef union {
    t_str	string;
    s_symbol	*symbol;
    s_chain	*chain;
    t_value	value;
    s_tfs	*tfs;
    s_var_map	*var_map;
    t_letter	*letter_string;
    s_pair	*pair;
    } YYSTYPE;
#define	ANY	258
#define	BAR	259
#define	BIARROW	260
#define	WORDBOUNDARY	261
#define	CONTEXTBOUNDARY	262
#define	CONCATBOUNDARY	263
#define	MORPHEMEBOUNDARY	264
#define	DOLLAR	265
#define	EQUAL	266
#define	LANGLE	267
#define	LARROW	268
#define	LBRA	269
#define	NOTEQUAL	270
#define	RANGLE	271
#define	RARROW	272
#define	RBRA	273
#define	COERCEARROW	274
#define	COLON	275
#define	SLASH	276
#define	ALPHABETS	277
#define	ATTRIBUTES	278
#define	CLASSES	279
#define	GRAMMAR	280
#define	LEXICON	281
#define	PAIRS	282
#define	SPELLING	283
#define	TYPES	284
#define	DNAME	285
#define	NAME	286
#define	STRING	287
#define	SNAME	288
#define	TNAME	289
#define	LSTRING	290


extern YYSTYPE yylval;
