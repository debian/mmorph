
/*  A Bison parser, made from user.y
 by  GNU Bison version 1.25
  */

#define YYBISON 1  /* Identify Bison output.  */

#define	ANY	258
#define	BAR	259
#define	BIARROW	260
#define	WORDBOUNDARY	261
#define	CONTEXTBOUNDARY	262
#define	CONCATBOUNDARY	263
#define	MORPHEMEBOUNDARY	264
#define	DOLLAR	265
#define	EQUAL	266
#define	LANGLE	267
#define	LARROW	268
#define	LBRA	269
#define	NOTEQUAL	270
#define	RANGLE	271
#define	RARROW	272
#define	RBRA	273
#define	COERCEARROW	274
#define	COLON	275
#define	SLASH	276
#define	ALPHABETS	277
#define	ATTRIBUTES	278
#define	CLASSES	279
#define	GRAMMAR	280
#define	LEXICON	281
#define	PAIRS	282
#define	SPELLING	283
#define	TYPES	284
#define	DNAME	285
#define	NAME	286
#define	STRING	287
#define	SNAME	288
#define	TNAME	289
#define	LSTRING	290

#line 1 "user.y"

/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
*/
#include "user.h"
#include "parse.h"

s_symbol	*defined_symbol;
static s_symbol	*largest_type_sym;
static t_card		 largest_type_card;
e_symbol_kind    defined_kind;
static s_symbol_set	*value_set;
static s_symbol_set	*variable_set;
static t_boolean	 variable_allowed;
static s_symbol	*current_attribute;
static s_type		*current_type;
static s_rule		*current_rule;
static s_tfs		*current_tfs;
static s_tfs		*common_tfs;
static s_chain		*current_chain;
static s_chain		*affix_tree_chain;
static int		 var_map_card;
static t_letter 	 *current_string;
static t_letter	 *current_base_form;
e_symbol_kind    current_letter_kind;
static s_pair		*current_pair;
static t_flag		 class_flags;
static t_flag		 current_pair_flags;
static int		 current_pair_width;
static s_bitmap	 *current_class;
static s_symbol	  fake_symbol;	/* used for pointing defined_symbol to */
static s_spell_instance *current_spell;
static s_bitmap	 *current_lexical_set;
static int		 current_pattern_length;
static s_chain		 *current_pattern;
static t_boolean	 within_focus;
static t_boolean	 has_concat_pos;

#define LETTER_STRING_SIZE_MAX	128
static t_letter	letter_string[LETTER_STRING_SIZE_MAX]; /* temp storage */
static int		letter_string_size; /* size of data in letter_string */

#line 47 "user.y"
typedef union {
    t_str	string;
    s_symbol	*symbol;
    s_chain	*chain;
    t_value	value;
    s_tfs	*tfs;
    s_var_map	*var_map;
    t_letter	*letter_string;
    s_pair	*pair;
    } YYSTYPE;
#ifndef YYDEBUG
#define YYDEBUG 1
#endif

#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		193
#define	YYFLAG		-32768
#define	YYNTBASE	36

#define YYTRANSLATE(x) ((unsigned)(x) <= 290 ? yytranslate[x] : 128)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
    26,    27,    28,    29,    30,    31,    32,    33,    34,    35
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     9,    12,    14,    18,    20,    22,    25,    27,    32,
    35,    36,    38,    40,    42,    45,    47,    51,    55,    57,
    59,    61,    63,    65,    67,    70,    72,    76,    79,    81,
    85,    90,    92,    94,    97,   100,   103,   105,   108,   110,
   113,   116,   119,   122,   125,   127,   130,   132,   135,   137,
   140,   142,   145,   146,   149,   150,   153,   155,   158,   159,
   161,   169,   171,   174,   176,   178,   180,   184,   186,   188,
   190,   193,   194,   197,   199,   201,   202,   204,   206,   208,
   210,   212,   213,   217,   220,   222,   224,   227,   229,   232,
   234,   237,   240,   241,   243,   246,   247,   250,   252,   259,
   261,   263,   266,   268,   270,   272,   275,   276,   278,   281,
   283,   287,   289,   292,   294,   298,   300,   304,   306,   308,
   310,   312,   316,   318,   320,   322,   325,   326,   329,   330,
   332,   335,   337,   341,   343,   346,   348,   350
};

static const short yyrhs[] = {   101,
    37,    42,    49,   121,   108,    77,    90,     0,    38,    70,
     0,    23,     0,    40,    20,    71,     0,    30,     0,    31,
     0,    43,    72,     0,    29,     0,    47,    20,    76,    45,
     0,    46,    76,     0,     0,     4,     0,    30,     0,    31,
     0,    50,    73,     0,    25,     0,    56,    20,    52,     0,
    54,    53,    68,     0,    69,     0,    57,     0,    13,     0,
    57,     0,    35,     0,    30,     0,    58,    59,     0,    34,
     0,    14,    74,    18,     0,    48,    61,     0,    63,     0,
    11,    10,    62,     0,    11,    10,    62,    63,     0,    31,
     0,    64,     0,    15,    65,     0,    11,    65,     0,    66,
    75,     0,    31,     0,     4,    66,     0,    54,     0,    54,
    54,     0,    69,    54,     0,    54,    69,     0,    55,    57,
     0,    70,    39,     0,    39,     0,    71,    41,     0,    41,
     0,    72,    44,     0,    44,     0,    73,    51,     0,    51,
     0,    74,    60,     0,     0,    75,    67,     0,     0,    76,
    48,     0,    48,     0,    78,    81,     0,     0,    28,     0,
    80,    20,    85,    82,    84,    83,    86,     0,    30,     0,
    81,    79,     0,    79,     0,    88,     0,    88,     0,     7,
    87,     7,     0,    17,     0,     5,     0,    19,     0,    86,
    57,     0,     0,    87,    89,     0,    89,     0,    87,     0,
     0,     8,     0,     9,     0,     6,     0,   115,     0,    91,
     0,     0,    91,    92,   100,     0,    93,   100,     0,    26,
     0,    26,     0,    95,    96,     0,    57,     0,    96,    97,
     0,    97,     0,    99,    98,     0,    11,    35,     0,     0,
    35,     0,   100,    94,     0,     0,   102,   103,     0,    22,
     0,   104,    20,   106,   105,    20,   106,     0,    30,     0,
    30,     0,   106,   107,     0,   107,     0,    31,     0,    32,
     0,   109,   110,     0,     0,    27,     0,   110,   111,     0,
   111,     0,   112,    20,   113,     0,    30,     0,   113,   114,
     0,   114,     0,   117,    21,   118,     0,   116,     0,   117,
    21,   118,     0,   116,     0,    31,     0,   119,     0,   119,
     0,    12,   120,    16,     0,    35,     0,    31,     0,     3,
     0,   120,    31,     0,     0,   122,   123,     0,     0,    24,
     0,   123,   124,     0,   124,     0,   125,    20,   126,     0,
    30,     0,   126,   127,     0,   127,     0,    31,     0,    32,
     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
   141,   148,   157,   165,   173,   183,   191,   213,   222,   240,
   243,   248,   254,   264,   272,   283,   293,   299,   305,   311,
   325,   331,   337,   344,   353,   368,   381,   387,   393,   397,
   401,   407,   439,   456,   463,   469,   475,   482,   488,   494,
   500,   506,   514,   523,   527,   533,   537,   543,   547,   553,
   557,   563,   569,   576,   580,   586,   594,   604,   614,   619,
   627,   646,   665,   669,   675,   687,   694,   706,   710,   714,
   721,   726,   733,   736,   741,   744,   749,   760,   767,   774,
   783,   786,   791,   795,   800,   805,   823,   833,   846,   849,
   854,   879,   884,   890,   896,   900,   906,   913,   950,   961,
   974,   989,   992,   997,  1012,  1035,  1043,  1048,  1056,  1060,
  1065,  1071,  1085,  1088,  1093,  1121,  1141,  1162,  1171,  1183,
  1191,  1199,  1204,  1208,  1233,  1239,  1247,  1254,  1262,  1266,
  1274,  1277,  1282,  1300,  1308,  1311,  1316,  1359
};
#endif


#if YYDEBUG != 0 || defined (YYERROR_VERBOSE)

static const char * const yytname[] = {   "$","error","$undefined.","ANY","BAR",
"BIARROW","WORDBOUNDARY","CONTEXTBOUNDARY","CONCATBOUNDARY","MORPHEMEBOUNDARY",
"DOLLAR","EQUAL","LANGLE","LARROW","LBRA","NOTEQUAL","RANGLE","RARROW","RBRA",
"COERCEARROW","COLON","SLASH","ALPHABETS","ATTRIBUTES","CLASSES","GRAMMAR","LEXICON",
"PAIRS","SPELLING","TYPES","DNAME","NAME","STRING","SNAME","TNAME","LSTRING",
"Start","AttDecl","AttHead","AttDef","AttNew","ValNew","TypeDecl","TypeHead",
"TypeDef","NoProjectAttOPTL","ProjSep","TypeNew","AttName","GramDecl","GramHead",
"Rule","RuleBody","Rewrite","VarTfs","AffixString","RuleNew","Tfs","TypeName",
"AttSpec","AttVal","ValSpec","VarName","ValSetSpec","ValSet","ValDisj","ValName",
"ValDisjRest","Rhs","Affix","AttDefPLUS","ValNewPLUS","TypeDefPLUS","RulePLUS",
"AttValSTAR","ValDisjRestSTAR","AttNamePLUS","SpellDeclOPT","SpellHead","SpellDef",
"SpellNew","SpellDefPLUS","LeftContext","RightContext","Focus","Arrow","ConstraintSTAR",
"PatternPLUS","PatternSTAR","Pattern","LexDeclSTAR","LexDeclPLUS","LexHead",
"FirstLexHead","LexDef","CommonTfs","LexicalPLUS","Lexical","BaseFormOPTL","LexNew",
"LexDefSTAR","AlphabetDecl","AlphabetHead","AlphabetDef","LexicalNew","SurfaceNew",
"LetterNewPLUS","LetterNew","PairDeclOPT","PairHead","PairDefPLUS","PairDef",
"PairNew","PairPLUS","Pair","PairPattern","PairName","SurfSequence","LexSequence",
"Sequence","LetterSTAR","ClassDeclOPT","ClassHead","ClassDefPLUS","ClassDef",
"ClassNew","ClassPLUS","Class", NULL
};
#endif

static const short yyr1[] = {     0,
    36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
    45,    46,    47,    48,    49,    50,    51,    52,    52,    52,
    53,    54,    55,    56,    57,    58,    59,    60,    61,    61,
    61,    62,    63,    64,    64,    65,    66,    67,    68,    68,
    68,    68,    69,    70,    70,    71,    71,    72,    72,    73,
    73,    74,    74,    75,    75,    76,    76,    77,    77,    78,
    79,    80,    81,    81,    82,    83,    84,    85,    85,    85,
    86,    86,    87,    87,    88,    88,    89,    89,    89,    89,
    90,    90,    91,    91,    92,    93,    94,    95,    96,    96,
    97,    98,    98,    99,   100,   100,   101,   102,   103,   104,
   105,   106,   106,   107,   107,   108,   108,   109,   110,   110,
   111,   112,   113,   113,   114,   114,   115,   115,   116,   117,
   118,   119,   119,   119,   119,   120,   120,   121,   121,   122,
   123,   123,   124,   125,   126,   126,   127,   127
};

static const short yyr2[] = {     0,
     8,     2,     1,     3,     1,     1,     2,     1,     4,     2,
     0,     1,     1,     1,     2,     1,     3,     3,     1,     1,
     1,     1,     1,     1,     2,     1,     3,     2,     1,     3,
     4,     1,     1,     2,     2,     2,     1,     2,     1,     2,
     2,     2,     2,     2,     1,     2,     1,     2,     1,     2,
     1,     2,     0,     2,     0,     2,     1,     2,     0,     1,
     7,     1,     2,     1,     1,     1,     3,     1,     1,     1,
     2,     0,     2,     1,     1,     0,     1,     1,     1,     1,
     1,     0,     3,     2,     1,     1,     2,     1,     2,     1,
     2,     2,     0,     1,     2,     0,     2,     1,     6,     1,
     1,     2,     1,     1,     1,     2,     0,     1,     2,     1,
     3,     1,     2,     1,     3,     1,     3,     1,     1,     1,
     1,     3,     1,     1,     1,     2,     0,     2,     0,     1,
     2,     1,     3,     1,     2,     1,     1,     1
};

static const short yydefact[] = {     0,
    98,     0,     0,     3,     0,     0,   100,    97,     0,     8,
     0,     0,     5,    45,     0,     2,     0,    16,   129,     0,
    13,    49,     0,     7,     0,    44,   104,   105,     0,   103,
   130,   107,     0,    24,    51,     0,    15,     0,    48,     6,
    47,     4,   101,     0,   102,   108,    59,     0,   134,   128,
   132,     0,     0,    50,    14,    57,    11,    46,     0,    60,
    82,     0,   112,   106,   110,     0,   131,     0,    26,    23,
    17,     0,     0,    20,     0,    19,    12,     9,     0,    56,
    99,    86,     1,    81,    96,    62,    64,     0,    58,   109,
     0,   137,   138,   133,   136,    21,     0,    43,    53,    25,
    10,    85,    96,    84,     0,    63,   125,   127,   119,   123,
   111,   114,   116,     0,   120,   135,    39,    22,    18,     0,
     0,    83,    88,    95,     0,    69,    68,    70,    76,     0,
   113,     0,    40,    42,    41,    27,     0,    52,    94,    87,
    90,    93,    79,    77,    78,     0,    75,    65,    74,    80,
   118,     0,   122,   126,   124,   115,   121,     0,     0,    28,
    29,    33,    89,     0,    91,     0,    76,    73,     0,     0,
    37,    35,    55,    34,    92,     0,    72,    66,   117,    32,
    30,    36,    67,    61,     0,    31,     0,    54,    71,    38,
     0,     0,     0
};

static const short yydefgoto[] = {   191,
     5,     6,    14,    15,    41,    11,    12,    22,    78,    79,
    23,    56,    19,    20,    35,    71,    97,    72,    73,    36,
   118,    75,   100,   138,   160,   181,   161,   162,   172,   173,
   188,   119,    76,    16,    42,    24,    37,   121,   182,    57,
    61,    62,    87,    88,    89,   146,   177,   167,   129,   184,
   147,   148,   149,    83,    84,   103,    85,   124,   125,   140,
   141,   165,   142,   104,     2,     3,     8,     9,    44,    29,
    30,    47,    48,    64,    65,    66,   111,   112,   150,   151,
   152,   156,   115,   130,    32,    33,    50,    51,    52,    94,
    95
};

static const short yypact[] = {     7,
-32768,    35,    -6,-32768,     6,    26,-32768,-32768,    53,-32768,
    38,    46,-32768,-32768,    57,    26,    29,-32768,    54,    49,
-32768,-32768,    60,    46,    50,-32768,-32768,-32768,    40,-32768,
-32768,    55,    56,-32768,-32768,    63,    49,    58,-32768,-32768,
-32768,    50,-32768,    64,-32768,-32768,    59,    61,-32768,    56,
-32768,    65,    33,-32768,-32768,-32768,    19,-32768,    29,-32768,
    62,    66,-32768,    61,-32768,    70,-32768,    43,-32768,-32768,
-32768,    79,    67,    80,    81,-32768,-32768,-32768,    58,-32768,
    29,-32768,-32768,    68,-32768,-32768,-32768,    77,    66,-32768,
     2,-32768,-32768,    43,-32768,-32768,    33,-32768,-32768,-32768,
    58,-32768,-32768,    67,    47,-32768,-32768,-32768,    78,-32768,
     2,-32768,-32768,    82,-32768,-32768,    33,-32768,-32768,    67,
    -1,    67,-32768,-32768,    69,-32768,-32768,-32768,    13,    12,
-32768,     3,-32768,-32768,-32768,-32768,    42,-32768,-32768,    69,
-32768,    87,-32768,-32768,-32768,    93,    13,-32768,-32768,-32768,
-32768,    84,-32768,-32768,-32768,-32768,-32768,     8,    71,-32768,
-32768,-32768,-32768,    72,-32768,    13,    13,-32768,     3,    75,
-32768,-32768,-32768,-32768,-32768,     1,-32768,-32768,-32768,-32768,
    44,   104,-32768,    67,    71,-32768,    71,-32768,-32768,-32768,
   109,   110,-32768
};

static const short yypgoto[] = {-32768,
-32768,-32768,    95,-32768,    73,-32768,-32768,    88,-32768,-32768,
-32768,   -56,-32768,-32768,    76,-32768,-32768,   -71,-32768,-32768,
   -53,-32768,-32768,-32768,-32768,-32768,   -67,-32768,   -43,   -70,
-32768,-32768,   -86,-32768,-32768,-32768,-32768,-32768,-32768,    39,
-32768,-32768,    30,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
   -46,   -45,  -135,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
   -19,-32768,-32768,    20,-32768,-32768,-32768,-32768,-32768,    74,
   -27,-32768,-32768,-32768,    83,-32768,-32768,    14,-32768,   -64,
   -49,   -42,  -129,-32768,-32768,-32768,-32768,    85,-32768,-32768,
    32
};


#define	YYLAST		147


static const short yytable[] = {    74,
    80,    45,   157,   107,   107,   107,   143,   183,   144,   145,
   120,   168,   108,   108,   108,   107,   136,   170,   143,    98,
   144,   145,    77,     7,   108,   117,   113,   153,     1,    55,
   134,   109,   109,   155,    10,   110,   110,   110,   171,   157,
   168,   114,   154,   109,    80,   133,   113,   110,   135,    55,
   123,   126,   158,    45,   185,    13,   159,     4,   159,    27,
    28,   114,    18,   127,   137,   128,    69,    70,   123,    43,
    27,    28,    17,    92,    93,    21,    25,    31,    34,    38,
    40,    46,    53,    59,    68,    49,    60,    82,    55,    91,
    63,    96,   -22,   102,    99,    86,   105,   164,  -124,   166,
    69,   171,   132,   139,   169,   180,   175,   187,   192,   193,
    26,    39,    54,   186,    58,   174,   190,   101,   106,   176,
   163,   178,   122,     0,   131,   116,   179,     0,     0,     0,
   189,     0,    81,     0,    67,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,    90
};

static const short yycheck[] = {    53,
    57,    29,   132,     3,     3,     3,     6,     7,     8,     9,
    97,   147,    12,    12,    12,     3,    18,    10,     6,    73,
     8,     9,     4,    30,    12,    97,    91,    16,    22,    31,
   117,    31,    31,    31,    29,    35,    35,    35,    31,   169,
   176,    91,    31,    31,   101,   117,   111,    35,   120,    31,
   104,     5,    11,    81,    11,    30,    15,    23,    15,    31,
    32,   111,    25,    17,   121,    19,    34,    35,   122,    30,
    31,    32,    20,    31,    32,    30,    20,    24,    30,    20,
    31,    27,    20,    20,    20,    30,    28,    26,    31,    20,
    30,    13,    13,    26,    14,    30,    20,    11,    21,     7,
    34,    31,    21,    35,    21,    31,    35,     4,     0,     0,
    16,    24,    37,   181,    42,   159,   187,    79,    89,   166,
   140,   167,   103,    -1,   111,    94,   169,    -1,    -1,    -1,
   184,    -1,    59,    -1,    50,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    64
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/usr/share/bison.simple"

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi)
#include <alloca.h>
#else /* not sparc */
#if defined (MSDOS) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
 #pragma alloca
#else /* not MSDOS, __TURBOC__, or _AIX */
#ifdef __hpux
#ifdef __cplusplus
extern "C" {
void *alloca (unsigned int);
};
#else /* not __cplusplus */
void *alloca ();
#endif /* not __cplusplus */
#endif /* __hpux */
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif /* alloca not defined.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	return(0)
#define YYABORT 	return(1)
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
int yyparse (void);
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(TO,FROM,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (to, from, count)
     char *to;
     char *from;
     int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (char *to, char *from, int count)
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 196 "/usr/share/bison.simple"

/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#ifdef __cplusplus
#define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#else /* not __cplusplus */
#define YYPARSE_PARAM_ARG YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#endif /* not __cplusplus */
#else /* not YYPARSE_PARAM */
#define YYPARSE_PARAM_ARG
#define YYPARSE_PARAM_DECL
#endif /* not YYPARSE_PARAM */

int
yyparse(YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
      yyss = (short *) alloca (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss, (char *)yyss1, size * sizeof (*yyssp));
      yyvs = (YYSTYPE *) alloca (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs, (char *)yyvs1, size * sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) alloca (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls, (char *)yyls1, size * sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 1:
#line 143 "user.y"
{

	;
    break;}
case 2:
#line 149 "user.y"
{
	    defined_symbol=NULL; /* end of definitions */
	    make_symbol_reference(&symbol_set[Attribute]);
	    if (debug & DEBUG_INIT)
		print_symbols_by_ref(&symbol_set[Attribute]);
	;
    break;}
case 3:
#line 158 "user.y"
{
	    defined_kind=Attribute;
	    crc32file_addn(&crc, (unsigned char *) "@Attributes ",
			   sizeof("@Attributes "));
	;
    break;}
case 4:
#line 166 "user.y"
{
	    make_symbol_reference(value_set);
	    link_attribute(value_set,yyvsp[-2].symbol);
	    defined_symbol=NULL; /* for error message */
	;
    break;}
case 5:
#line 174 "user.y"
{
	    yyval.symbol=*new_symbol(yyvsp[0].string,&symbol_set[Attribute]);
	    value_set=&yyval.symbol->data->attribute.value_set;
	    defined_symbol=yyval.symbol;
	    crc32file_addn(&crc, (unsigned char *) yyvsp[0].string, (int) strlen(yyvsp[0].string));
	    crc32file_add(&crc, (unsigned char) ':');
	;
    break;}
case 6:
#line 184 "user.y"
{
	    (void) new_symbol(yyvsp[0].string,value_set);
	    crc32file_addn(&crc, (unsigned char *) yyvsp[0].string, (int) strlen(yyvsp[0].string));
	    crc32file_add(&crc, (unsigned char) ' ');
	;
    break;}
case 7:
#line 192 "user.y"
{
	    defined_symbol=NULL; /* end of definitions */
	    make_symbol_reference(&symbol_set[Type]);
	    largest_type=largest_type_sym->ordinal;
	    if (debug & DEBUG_INIT)
		print_symbols_by_ref(&symbol_set[Type]);
	    /* TODO: move this to end of spelling rules
               when looking up unknown words works.
	     */
	    if (db_operation & Lookup) {
		crc32file_hash(&crc, crc_hash);
		parsing=FALSE; /* change format of fatal errors */
		tfs_table_init(db_operation, gram_file_name, db_file_name,
			       crc_hash);
		parsing=TRUE;
	    }
	    if (db_operation == Lookup)
		YYACCEPT; /* skip the rest of the file */
	;
    break;}
case 8:
#line 214 "user.y"
{
	    defined_kind=Type;
	    largest_type_card = 0;
	    crc32file_addn(&crc, (unsigned char *) "@Types ",
			   sizeof("@Types "));
	;
    break;}
case 9:
#line 223 "user.y"
{
	    if (current_type->project_card==0)
		current_type->project_card=current_type->card;
	    if (current_type->card > largest_type_card) {
		largest_type_sym = defined_symbol;
		largest_type_card = current_type->card;
	    }
	    current_type->attribute_ref=
		  (s_symbol **) new_ref((int) current_type->card,
					 map_chain,
					 (t_ptr) current_chain,
					 ref_is_item);
	    free_chain(current_chain);
	    defined_symbol=NULL; /* for error message */
	;
    break;}
case 10:
#line 241 "user.y"
{
		;
    break;}
case 11:
#line 244 "user.y"
{
		;
    break;}
case 12:
#line 249 "user.y"
{
	    current_type->project_card= current_type->card;
	    crc32file_add(&crc, (unsigned char) '|');
	;
    break;}
case 13:
#line 255 "user.y"
{
	    defined_symbol = *new_symbol(yyvsp[0].string,&symbol_set[Type]);
	    current_type = (s_type *) defined_symbol->data;
	    current_chain = NULL;
	    crc32file_addn(&crc, (unsigned char *) yyvsp[0].string, (int) strlen(yyvsp[0].string));
	    crc32file_add(&crc, (unsigned char) ':');
	;
    break;}
case 14:
#line 265 "user.y"
{
	    yyval.symbol=*find_symbol(yyvsp[0].string,&symbol_set[Attribute]);
	    current_attribute=yyval.symbol;
	    value_set= &((yyval.symbol)->data->attribute.value_set);
	;
    break;}
case 15:
#line 273 "user.y"
{
	    defined_symbol=NULL; /* end of definitions */
	    make_symbol_reference(&symbol_set[Rule]);
	    if (debug & DEBUG_INIT)
		print_symbols_by_ref(&symbol_set[Rule]);
	    if (! parse_only)
		prepare_rules();
	;
    break;}
case 16:
#line 284 "user.y"
{
	    defined_kind=Rule;
	    goal_card = 0;
	    /* tell tokenizer to interpret strings as lexical strings */
	    current_letter_kind=Lexical_Letter;
	    current_letter_map=lexical_letter_map;
	;
    break;}
case 17:
#line 294 "user.y"
{
	    defined_symbol=NULL; /* for error message */
	;
    break;}
case 18:
#line 300 "user.y"
{
		make_symbol_reference(variable_set);
		current_rule->instance->lhs=yyvsp[-2].tfs;
		adjust_var_map(variable_set->ref,current_rule->instance);
	    ;
    break;}
case 19:
#line 306 "user.y"
{
		variable_set = NULL; /* no variable to handle */
		current_rule->instance->rule_kind = Lexical;
		current_rule->instance->lhs=yyvsp[0].tfs;
	    ;
    break;}
case 20:
#line 312 "user.y"
{
		current_rule->instance->rule_kind = Goal;
		goal_card++;
		if (yyvsp[0].tfs->var_map != NULL) /* TODO catch this earlier */
		    fatal_error("variables not allowed in goal rule");
		current_rule->instance->first=yyvsp[0].tfs;
		current_rule->instance->lex= (t_letter *) "";/* empty string */
		current_rule->instance->lhs=new_tfs(yyvsp[0].tfs->type);
		fill_tfs_with_variables(current_rule->instance->lhs);
		ident_var_map(current_rule->instance->lhs);
	    ;
    break;}
case 21:
#line 326 "user.y"
{

	    ;
    break;}
case 22:
#line 332 "user.y"
{
		yyval.tfs=yyvsp[0].tfs;
	    ;
    break;}
case 23:
#line 338 "user.y"
{
		variable_allowed=FALSE;
		yyval.letter_string=yyvsp[0].letter_string;
	    ;
    break;}
case 24:
#line 345 "user.y"
{
	    defined_symbol = *new_symbol(yyvsp[0].string,&symbol_set[Rule]);
	    current_rule = (s_rule *)defined_symbol->data;
	    variable_set = &(current_rule->variable_set);
	    variable_allowed=TRUE;
	;
    break;}
case 25:
#line 354 "user.y"
{
	    yyval.tfs = yyvsp[-1].tfs; /* that's the tfs */
	    if (yyvsp[0].chain == NULL)
		yyval.tfs->var_map = NULL;
	    else {
		yyval.tfs->var_map = (s_var_map **) new_ref(var_map_card,
						     map_chain,
						     (t_ptr) yyvsp[0].chain,
						     ref_is_item);
		free_chain(yyvsp[0].chain);
	    }
	;
    break;}
case 26:
#line 369 "user.y"
{
	    s_symbol *symbol;

	    symbol = *find_symbol(yyvsp[0].string,&symbol_set[Type]);
	    current_type = (s_type *) symbol->data;
	    yyval.tfs=new_tfs(symbol->ordinal);
	    current_tfs=yyval.tfs;
	    /* fill the type feature structure with full sets */
	    fill_tfs_with_variables(yyval.tfs);
	;
    break;}
case 27:
#line 382 "user.y"
{
	    yyval.chain=yyvsp[-1].chain;
	;
    break;}
case 28:
#line 388 "user.y"
{
	    yyval.var_map=yyvsp[0].var_map;
	;
    break;}
case 29:
#line 394 "user.y"
{
	    yyval.var_map=NULL;
	;
    break;}
case 30:
#line 398 "user.y"
{
	    yyval.var_map=yyvsp[0].var_map;
	;
    break;}
case 31:
#line 402 "user.y"
{
	    yyval.var_map=yyvsp[-1].var_map;
	;
    break;}
case 32:
#line 408 "user.y"
{
	    int pos;
	    s_symbol *variable;

	    pos = reference_position((t_ptr *) current_type->attribute_ref,
			             (t_ptr) current_attribute);
	    if ( pos < 0 )
		fatal_error(
		   "attribute \"%s\" not declared for type \"%s\"",
		    current_attribute->name,
		    symbol_set[Type].ref[current_tfs->type]->name);
	    if (! variable_allowed)
		fatal_error(
		    "a variable is not allowed in this context");
	    variable = *new_symbol(yyvsp[0].string,variable_set);
	    if (variable->data->variable.attribute_link == NULL)
		variable->data->variable.attribute_link=current_attribute;
	    else
		if (variable->data->variable.attribute_link
			!= current_attribute)
		    fatal_error("variable $%s %s \"%s\" and \"%s\"",
			yyvsp[0].string,
			"is used for incompatible attributes:",
			variable->data->variable.attribute_link->name,
			current_attribute->name);
	    MY_MALLOC(yyval.var_map,s_var_map);
	    yyval.var_map->foreign_index = (t_index) variable->ordinal;
	    yyval.var_map->local_index = (t_index) pos;
	;
    break;}
case 33:
#line 440 "user.y"
{
	    int pos;

	    pos = reference_position((t_ptr *) current_type->attribute_ref,
			             (t_ptr) current_attribute);
	    if ( pos < 0 )
		fatal_error(
		   "attribute \"%s\" not declared for type \"%s\"",
		    current_attribute->name,
		    symbol_set[Type].ref[current_tfs->type]->name);
	    current_tfs->att_list[pos] &= yyvsp[0].value;
	    if (current_tfs->att_list[pos] == NO_BITS)
		fatal_error("value set complement for \"%s\" is empty",
			    current_attribute->name);
	;
    break;}
case 34:
#line 457 "user.y"
{
	    yyval.value = ~ (yyvsp[0].value);
	    if (yyval.value == NO_BITS)
	        /* occurs only when there is MAXVAL values for attribute */
		fatal_error("value set complement is empty");
	;
    break;}
case 35:
#line 464 "user.y"
{
	    yyval.value = yyvsp[0].value;
	;
    break;}
case 36:
#line 470 "user.y"
{
	    yyval.value = (yyvsp[-1].value | yyvsp[0].value);
	;
    break;}
case 37:
#line 476 "user.y"
{
	    
	    yyval.value = BIT_N((*find_symbol(yyvsp[0].string,value_set))->ordinal);
	;
    break;}
case 38:
#line 483 "user.y"
{
	    yyval.value = yyvsp[0].value;
	;
    break;}
case 39:
#line 489 "user.y"
{
	    current_rule->instance->rule_kind=Unary;
	    current_rule->instance->first=yyvsp[0].tfs;
	    current_rule->instance->second=NULL;
	;
    break;}
case 40:
#line 495 "user.y"
{
	    current_rule->instance->rule_kind=Binary;
	    current_rule->instance->first=yyvsp[-1].tfs;
	    current_rule->instance->second=yyvsp[0].tfs;
	;
    break;}
case 41:
#line 501 "user.y"
{
	    current_rule->instance->rule_kind=Prefix;
	    current_rule->instance->first=yyvsp[0].tfs;
	    current_rule->instance->second=yyvsp[-1].tfs;
	;
    break;}
case 42:
#line 507 "user.y"
{
	    current_rule->instance->rule_kind=Suffix;
	    current_rule->instance->first=yyvsp[-1].tfs;
	    current_rule->instance->second=yyvsp[0].tfs;
	;
    break;}
case 43:
#line 515 "user.y"
{
	    /* affix tfs cannot have variables; see adjust_var_map() */
	    current_rule->instance->lex=yyvsp[-1].letter_string;
	    current_rule->instance->lex_length=strlen((char *) yyvsp[-1].letter_string);
	    yyval.tfs=yyvsp[0].tfs;
	    variable_allowed = TRUE; /* useful only for prefix */
	;
    break;}
case 44:
#line 524 "user.y"
{

	;
    break;}
case 45:
#line 528 "user.y"
{

	;
    break;}
case 46:
#line 534 "user.y"
{

	;
    break;}
case 47:
#line 538 "user.y"
{

	;
    break;}
case 48:
#line 544 "user.y"
{

	;
    break;}
case 49:
#line 548 "user.y"
{

	;
    break;}
case 50:
#line 554 "user.y"
{

	;
    break;}
case 51:
#line 558 "user.y"
{

	;
    break;}
case 52:
#line 564 "user.y"
{
	    var_map_card++;
	    if (yyvsp[0].var_map != NULL)
	        yyval.chain = insert_chain_link(yyvsp[-1].chain, (t_ptr) yyvsp[0].var_map);
	;
    break;}
case 53:
#line 570 "user.y"
{
	    var_map_card=0;
	    yyval.chain=NULL;
	;
    break;}
case 54:
#line 577 "user.y"
{
	    yyval.value = ( yyvsp[-1].value | yyvsp[0].value );
	;
    break;}
case 55:
#line 581 "user.y"
{
	    yyval.value = 0L;
	;
    break;}
case 56:
#line 587 "user.y"
{
	    current_type->card++;
	    current_chain=insert_chain_link(current_chain, (t_ptr) yyvsp[0].symbol);
	    crc32file_addn(&crc, (unsigned char *) yyvsp[0].symbol->name,
			   (int) strlen(yyvsp[0].symbol->name));
	    crc32file_add(&crc, (unsigned char) ' ');
	;
    break;}
case 57:
#line 595 "user.y"
{
	    current_type->card++;
	    current_chain=insert_chain_link(current_chain, (t_ptr) yyvsp[0].symbol);
	    crc32file_addn(&crc, (unsigned char *) yyvsp[0].symbol->name,
			   (int) strlen(yyvsp[0].symbol->name));
	    crc32file_add(&crc, (unsigned char) ' ');
	;
    break;}
case 58:
#line 605 "user.y"
{
	    defined_symbol=NULL; /* end of definitions */
	    make_symbol_reference(&symbol_set[Spelling]);
	    if (debug & DEBUG_INIT)
		print_symbols_by_ref(&symbol_set[Spelling]);
	    max_spell_length=max_left_context_length + max_rest_length;
	    if (!parse_only)
		prepare_spell();
	;
    break;}
case 59:
#line 615 "user.y"
{
	;
    break;}
case 60:
#line 620 "user.y"
{
	    defined_kind=Spelling;
	    max_left_context_length=0;
	    max_rest_length=0;
	;
    break;}
case 61:
#line 629 "user.y"
{
	    current_spell->left_context
		= reverse_chain(current_spell->left_context);
	    if (! has_concat_pos)
		current_spell->concat_pos = NO_CONCAT; /* impossible value */
	    defined_symbol=NULL; /* for error message */
	    max_left_context_length 
		= MAX(current_spell->left_context_length,
		      max_left_context_length);
	    max_rest_length 
		= MAX(current_spell->focus_length
		      + current_spell->right_context_length,
		      max_rest_length);
	    current_spell->constraint = yyvsp[0].chain;
	;
    break;}
case 62:
#line 647 "user.y"
{
	    defined_symbol = *new_symbol(yyvsp[0].string,&symbol_set[Spelling]);
	    current_spell
		= defined_symbol->data->spelling.instance;
	    current_spell->spell_link=defined_symbol;
	    /* next letter will be found in surface alphabet */
	    current_letter_kind=Surface_Letter;
	    current_letter_map=surface_letter_map;
	    current_pair_flags=BOTH_LETTERS; /* for next explicit Pair */
	    has_concat_pos=FALSE;
	    current_spell->concat_pos=0;
	    current_pattern=NULL; /* prepare for left context */
	    current_pattern_length=0;
	    within_focus=FALSE;
	;
    break;}
case 63:
#line 666 "user.y"
{

	;
    break;}
case 64:
#line 670 "user.y"
{

	;
    break;}
case 65:
#line 676 "user.y"
{
		current_spell->left_context=current_pattern;
		current_spell->left_context_length=current_pattern_length;
		if (has_concat_pos)
		    current_spell->concat_pos -= current_pattern_length;
		current_pattern=NULL; /* prepare for focus */
		current_pattern_length=0;
		within_focus=TRUE;
	    ;
    break;}
case 66:
#line 688 "user.y"
{
		current_spell->right_context=current_pattern;
		current_spell->right_context_length=current_pattern_length;
	    ;
    break;}
case 67:
#line 695 "user.y"
{
		current_spell->focus = current_pattern;
		current_spell->focus_length=current_pattern_length;
		if (! has_concat_pos)
		    current_spell->concat_pos += current_pattern_length;
		current_pattern = NULL; /* prepare for right context */
		current_pattern_length=0;
		within_focus=FALSE;
	    ;
    break;}
case 68:
#line 707 "user.y"
{
	    current_spell->kind= Optional;
	;
    break;}
case 69:
#line 711 "user.y"
{
	    current_spell->kind= Obligatory;
	;
    break;}
case 70:
#line 715 "user.y"
{
	    current_spell->kind = Coerce;
	;
    break;}
case 71:
#line 722 "user.y"
{
	    yyval.chain = insert_chain_link(yyvsp[-1].chain, (t_ptr) yyvsp[0].tfs);
	    current_spell->constraint_card++;
	;
    break;}
case 72:
#line 727 "user.y"
{
	    current_spell->constraint_card = 0;
	    yyval.chain = NULL;
	;
    break;}
case 73:
#line 734 "user.y"
{
	    ;
    break;}
case 74:
#line 737 "user.y"
{
	    ;
    break;}
case 75:
#line 742 "user.y"
{
	    ;
    break;}
case 76:
#line 745 "user.y"
{
	    ;
    break;}
case 77:
#line 750 "user.y"
{
		if (has_concat_pos)
		    fatal_error("concatenation position already specified");
		has_concat_pos=TRUE;
		current_spell->concat_pos += current_pattern_length;
		current_pattern
		    = insert_chain_link(current_pattern,
					(t_ptr) morpheme_boundary_pair);
		current_pattern_length++;
	    ;
    break;}
case 78:
#line 761 "user.y"
{
		current_pattern
		    = insert_chain_link(current_pattern,
					(t_ptr) morpheme_boundary_pair);
		current_pattern_length++;
	    ;
    break;}
case 79:
#line 768 "user.y"
{
		current_pattern
		    = insert_chain_link(current_pattern,
					(t_ptr) word_boundary_pair);
		current_pattern_length++;
	    ;
    break;}
case 80:
#line 775 "user.y"
{
		current_pattern
		    = insert_chain_link(current_pattern, (t_ptr) yyvsp[0].pair);
		current_pattern_length += lexical_width(yyvsp[0].pair);
	    ;
    break;}
case 81:
#line 784 "user.y"
{
		;
    break;}
case 82:
#line 787 "user.y"
{
		;
    break;}
case 83:
#line 792 "user.y"
{

	;
    break;}
case 84:
#line 796 "user.y"
{
	;
    break;}
case 85:
#line 801 "user.y"
{
	;
    break;}
case 86:
#line 806 "user.y"
{
	    defined_kind = Lexicon;
	    variable_set=NULL; /* prevent variables in lexical Tfs */
	    variable_allowed=FALSE;
	    if ((db_operation & Update)) {
	 	/* options mentionned -a */
		augmenting=TRUE;
		initfile(augment_file_name); /* splice in file now */
	    }
	    /* otherwise splice it at the end of this file, cf tokenize.l */
	    /* tell tokenizer to interpret strings as lexical strings */
	    current_letter_kind=Lexical_Letter;
	    current_letter_map=lexical_letter_map;
	    normalize = normalize_flag;
	;
    break;}
case 87:
#line 824 "user.y"
{
	    free_tfs(common_tfs);
	    if (! parse_only) {
		map_chain(affix_tree_chain, free_affix_tree);
		free_chain(affix_tree_chain);
	    }
	;
    break;}
case 88:
#line 834 "user.y"
{
	    common_tfs = yyvsp[0].tfs;
	    if (! parse_only)
		affix_tree_chain = build_affix_trees(common_tfs);
	    else
		if (normalize) {
		    print_out("\t");
		    print_tfs1(outfile, common_tfs, FALSE, FALSE, FALSE);
		}
	  ;
    break;}
case 89:
#line 847 "user.y"
{
	    ;
    break;}
case 90:
#line 850 "user.y"
{
	    ;
    break;}
case 91:
#line 855 "user.y"
{
	    parsing=FALSE;
	    if (! parse_only)
		generate_surface(current_string, current_base_form,
			         affix_tree_chain);
	    else
		if (normalize) {
		    print_out("\"");
		    print_string(outfile, current_string);
		    if (current_string != current_base_form
		        && strcmp((char *) current_string,
			          (char *) current_base_form)) {
			print_out("\" = \"");
			print_string(outfile, current_base_form);
		    }
		    print_out("\"\n");
		}
	    parsing=TRUE;
	    if (current_string != current_base_form)
		MY_FREE(current_base_form);
	    MY_FREE(current_string);
	;
    break;}
case 92:
#line 880 "user.y"
{
		/* TODO should not be in the lexical alphabet but user */
		current_base_form = yyvsp[0].letter_string;
	    ;
    break;}
case 93:
#line 885 "user.y"
{
		current_base_form = current_string;
	    ;
    break;}
case 94:
#line 891 "user.y"
{
	    /* save string */
	    current_string = yyvsp[0].letter_string;
	;
    break;}
case 95:
#line 897 "user.y"
{

	;
    break;}
case 96:
#line 901 "user.y"
{

	;
    break;}
case 97:
#line 907 "user.y"
{
		defined_symbol=NULL; /* end of definitions */
		allow_string_letters=FALSE;
	     ;
    break;}
case 98:
#line 914 "user.y"
{
		t_str filler_symbol_name;

		defined_kind=Alphabet;
		init_symbol_set(&symbol_set[Pair], Lexical_Letter);
		/* fill the slot with ordinal number 0 with a dummy symbol
		   so that letter strings can also terminate with 0;
		   Use strdup, just in case the symbol is freed one day.
                 */
		MY_STRDUP(filler_symbol_name,""); /* impossible symbol name */
		(void) new_symbol(filler_symbol_name,&symbol_set[Pair]);
		/*  declare word boundary and morpheme boundary */
		MY_STRDUP(filler_symbol_name," + "); /* unlikely symbol name */
		morpheme_boundary_pair
		    = (s_pair *) (*new_symbol(filler_symbol_name,
				   &symbol_set[Pair]))->data;
		morpheme_boundary = morpheme_boundary_pair->lexical;
		/* tinker with the symbol table */
		morpheme_boundary_pair->surface= empty_string;
		first_lexical_letter=*morpheme_boundary;
		MY_STRDUP(filler_symbol_name," ~ "); /* unlikely symbol name */
		word_boundary_pair
		    = (s_pair *) (*new_symbol(filler_symbol_name,
				   &symbol_set[Pair]))->data;
		word_boundary=word_boundary_pair->lexical;
		/* tinker with the symbol table */
		word_boundary_pair->surface= empty_string;
		allow_string_letters=TRUE; /* allow ASTRING strings */
		MY_CALLOC(surface_letter_map,LETTER_MAP_SIZE,t_letter);
		MY_CALLOC(lexical_letter_map,LETTER_MAP_SIZE,t_letter);
		MY_CALLOC(letter_to_char_map,LETTER_MAP_SIZE,unsigned char);
		crc32file_addn(&crc, (unsigned char *) "@Alphabets ",
		               sizeof("@Alphabets "));
	     ;
    break;}
case 99:
#line 951 "user.y"
{
		MY_FREE(fake_symbol.name);
		alphabet_size=symbol_set[Pair].card;
		make_symbol_reference(&symbol_set[Pair]);
		if (debug & DEBUG_INIT) {
		    print_symbols_by_ref(&symbol_set[Pair]);
		}
	     ;
    break;}
case 100:
#line 962 "user.y"
{
		/* don't care about name, first is lexical, second surface */
		fake_symbol.name=yyvsp[0].string;
		fake_symbol.ordinal=0;
		defined_symbol=&fake_symbol;
		symbol_set[Pair].kind = Lexical_Letter;
		current_letter_map=lexical_letter_map;
		crc32file_addn(&crc, (unsigned char *) "Lexical:",
			       sizeof("Lexical:"));
	     ;
    break;}
case 101:
#line 975 "user.y"
{
		/* don't care about name, first is lexical, second surface */
		defined_kind=Alphabet;
		MY_FREE(fake_symbol.name);
		fake_symbol.name=yyvsp[0].string;
		fake_symbol.ordinal=0;
		symbol_set[Pair].kind = Surface_Letter;
		current_letter_map=surface_letter_map;
		lexical_alphabet_size=symbol_set[Pair].card;
		crc32file_addn(&crc, (unsigned char *) "Surface:",
			       sizeof("Surface:"));
	     ;
    break;}
case 102:
#line 990 "user.y"
{
	    ;
    break;}
case 103:
#line 993 "user.y"
{
	    ;
    break;}
case 104:
#line 998 "user.y"
{
		s_symbol *symbol;
		int	  length;

		symbol= *new_symbol(yyvsp[0].string,&symbol_set[Pair]);
		if ((length = strlen(symbol->name)) == 1) {
		    current_letter_map[(unsigned char) *symbol->name]
			    = (t_letter) symbol->ordinal;
		    letter_to_char_map[symbol->ordinal]
			    = (unsigned char) *symbol->name;
		}
		crc32file_addn(&crc, (unsigned char *) yyvsp[0].string , (int) length);
		crc32file_add(&crc, (unsigned char) ' ');
	    ;
    break;}
case 105:
#line 1013 "user.y"
{ 	/* allow special characters to be declared  as letter */
		/* TODO: cleanup the cases where this name could be printed */
		s_symbol *symbol;
		
		symbol= *new_symbol(yyvsp[0].string,&symbol_set[Pair]);
		if (strlen(symbol->name) != 1)
			fatal_error("%s %s %s",
			    "Sorry: cannot accept string definition of",
			    symbol_kind_str[current_letter_kind],
			    "longer than one character");
		 else {
		    current_letter_map[(unsigned char) *symbol->name]
			    = (t_letter) symbol->ordinal;
		    letter_to_char_map[symbol->ordinal]
			    = (unsigned char) *symbol->name;
		}
		crc32file_add(&crc, (unsigned char) '"');
		crc32file_add(&crc, (unsigned char) *yyvsp[0].string);
		crc32file_add(&crc, (unsigned char) '"');
	    ;
    break;}
case 106:
#line 1036 "user.y"
{
		defined_symbol=NULL; /* end of definitions */
		    if (debug & DEBUG_INIT) { /* TODO change this */
			print_symbol_table(&symbol_set[Pair]);
		    }
		MY_FREE(current_lexical_set);
	    ;
    break;}
case 107:
#line 1044 "user.y"
{
	    ;
    break;}
case 108:
#line 1049 "user.y"
{
		defined_kind=Pair;
		symbol_set[Pair].kind = Pair; 
		current_lexical_set=new_bitmap((long) alphabet_size);
	    ;
    break;}
case 109:
#line 1057 "user.y"
{
		
	    ;
    break;}
case 110:
#line 1061 "user.y"
{
	    ;
    break;}
case 111:
#line 1066 "user.y"
{
		defined_symbol=NULL; /* for error message */
	    ;
    break;}
case 112:
#line 1072 "user.y"
{
		defined_symbol = *new_symbol(yyvsp[0].string,&symbol_set[Pair]);

		current_pair = (s_pair *) defined_symbol->data;
		/* next letter will be found in surface alphabet */
		current_letter_kind=Surface_Letter;
		current_letter_map=surface_letter_map;
		current_pair_flags=BOTH_LETTERS; /* for next explicit Pair */
		empty_bitmap(current_lexical_set);
		current_pair_width=-1;
	    ;
    break;}
case 113:
#line 1086 "user.y"
{
	    ;
    break;}
case 114:
#line 1089 "user.y"
{
	    ;
    break;}
case 115:
#line 1094 "user.y"
{
		int lexical_width;

		if (current_pair_flags & LEXICAL_IS_CLASS)
		    lexical_width=1;
		else 
		    lexical_width=strlen((char *) yyvsp[0].letter_string);
		if (lexical_width > 1) { /* TODO: print pair */
		    fatal_error("%s\nthe lexical part %s",
			    "Sorry, not yet implemented.", 
			    "of the pair cannot be longer than one symbol");
		}
		if (current_pair_width == -1)
		    current_pair_width = lexical_width;
		else
		    if (current_pair_width != lexical_width)
			fatal_error("%s %s",
				    "you cannot mix pairs with different",
				    "lexical lengths");
		if (lexical_width==0
		    && !(current_pair_flags & SURFACE_IS_CLASS)
		    && *(yyvsp[-2].letter_string) == NUL_LETTER)
		    fatal_error("empty pair <>/<> is not allowed");
		add_explicit_pair(current_pair,yyvsp[-2].letter_string,yyvsp[0].letter_string,current_pair_flags,
				  current_lexical_set);
		current_pair_flags=BOTH_LETTERS; /* restore for next */
	    ;
    break;}
case 116:
#line 1122 "user.y"
{
		int pair_width;

		/* TODO factorize this code and similar above */
		if (yyvsp[0].symbol == defined_symbol)
		    fatal_error("pair name is used in its own definition");
		pair_width=lexical_width((s_pair *) yyvsp[0].symbol->data);
		if (current_pair_width == -1)
		    current_pair_width = pair_width;
		else
		    if (current_pair_width != pair_width)
			fatal_error("%s %s",
				    "you cannot mix pairs with different",
				    "lexical lengths");
		add_pair(current_pair, (s_pair *) yyvsp[0].symbol->data,
			 current_lexical_set);
	    ;
    break;}
case 117:
#line 1142 "user.y"
{ /* similar to Pair above */
		int lexical_width;

		if (! (current_pair_flags & LEXICAL_IS_CLASS)) {
		    lexical_width=strlen((char *) yyvsp[0].letter_string);
		    if (lexical_width > 1) /* TODO: print pair */
			fatal_error("%s\nthe lexical part of th pair %s",
				"Sorry, not yet implemented.", 
				"cannot be longer than one symbol");
		    else
			if (lexical_width == 0
			    && ! (current_pair_flags & LEXICAL_IS_CLASS)
			    && *(yyvsp[-2].letter_string) == NUL_LETTER)
			    fatal_error("empty pair <>/<> is not allowed");
		}
		yyval.pair=new_pair(yyvsp[-2].letter_string,yyvsp[0].letter_string,current_pair_flags,(s_pair *) NULL);
		if (within_focus)
		    is_pair_concrete(yyval.pair);
		current_pair_flags=BOTH_LETTERS; /* restore for next */
	    ;
    break;}
case 118:
#line 1163 "user.y"
{
		yyval.pair= (s_pair *) yyvsp[0].symbol->data;
		if (within_focus)
		    is_pair_concrete(yyval.pair);
	    ;
    break;}
case 119:
#line 1172 "user.y"
{
		yyval.symbol= *find_symbol(yyvsp[0].string,&symbol_set[Pair]);
		if (! (yyval.symbol->kind == Pair
		       || yyval.symbol->kind == Pair_Letter
		       || yyval.symbol->kind == Pair_Class))
		    fatal_error("%s \"%s\"",
				"There is no pair associated to symbol",
				yyval.symbol->name);
	    ;
    break;}
case 120:
#line 1184 "user.y"
{
		current_letter_kind=Lexical_Letter; /* alternate */
		current_letter_map=lexical_letter_map;
		yyval.letter_string=yyvsp[0].letter_string;
	    ;
    break;}
case 121:
#line 1192 "user.y"
{
		current_letter_kind=Surface_Letter; /* alternate */
		current_letter_map=surface_letter_map;
		yyval.letter_string=yyvsp[0].letter_string;
	    ;
    break;}
case 122:
#line 1200 "user.y"
{
		letter_string[letter_string_size]= NUL_LETTER;
		MY_STRDUP(yyval.letter_string,letter_string);
	    ;
    break;}
case 123:
#line 1205 "user.y"
{
		yyval.letter_string= (t_letter *) yyvsp[0].letter_string;
	    ;
    break;}
case 124:
#line 1209 "user.y"
{
		s_symbol   *pair_symbol;

		pair_symbol = *find_symbol(yyvsp[0].string, &symbol_set[Pair]);
		if (pair_symbol->kind== Pair
		    || (current_letter_kind == Surface_Letter
			&& (pair_symbol->kind == Lexical_Letter
			    || pair_symbol->kind == Lexical_Class))
		    || (current_letter_kind == Lexical_Letter
			&& (pair_symbol->kind == Surface_Letter
			    || pair_symbol->kind == Surface_Class)))
		    fatal_error("symbol \"%s\" is not a %s",
				pair_symbol->name,
				symbol_kind_str[current_letter_kind]);
		if (pair_symbol->data->pair.pair_flags == BOTH_CLASSES)
		    if (current_letter_kind == Surface_Letter)
			current_pair_flags |= SURFACE_IS_CLASS;
		    else
			current_pair_flags |= LEXICAL_IS_CLASS;
		if (current_letter_kind == Surface_Letter)
		    yyval.letter_string = pair_symbol->data->pair.surface;
		else
		    yyval.letter_string = pair_symbol->data->pair.lexical;
	    ;
    break;}
case 125:
#line 1234 "user.y"
{
		yyval.letter_string = letter_any;
	    ;
    break;}
case 126:
#line 1240 "user.y"
{
		if (letter_string_size >= LETTER_STRING_SIZE_MAX - 1)
		    fatal_error("symbol sequence too long, maximum=%d",
				LETTER_STRING_SIZE_MAX - 1);
		letter_string[letter_string_size++]
		    = *find_letter(yyvsp[0].string, current_letter_kind);
	    ;
    break;}
case 127:
#line 1248 "user.y"
{
		letter_string_size=0;
	    ;
    break;}
case 128:
#line 1255 "user.y"
{
		    defined_symbol=NULL; /* end of definitions */
		    allow_string_letters=FALSE;
		    if (debug & DEBUG_INIT) { /* TODO change this */
			print_symbol_table(&symbol_set[Pair]);
		    }
		;
    break;}
case 129:
#line 1263 "user.y"
{
		;
    break;}
case 130:
#line 1267 "user.y"
{
		defined_kind=Pair_Class;
		symbol_set[Pair].kind = Pair_Class;
		allow_string_letters=TRUE;
	    ;
    break;}
case 131:
#line 1275 "user.y"
{
	    ;
    break;}
case 132:
#line 1278 "user.y"
{
	    ;
    break;}
case 133:
#line 1283 "user.y"
{
		if (class_flags & SURFACE_FLAG)
		    if (class_flags & LEXICAL_FLAG) {
			defined_symbol->kind=Pair_Class;
			defined_symbol->data->class.lexical = current_class;
		    }
		    else
			defined_symbol->kind=Surface_Class;
		else {
		    defined_symbol->kind = Lexical_Class;
		    defined_symbol->data->class.lexical = current_class;
		    defined_symbol->data->class.surface = NULL;
		}
		defined_symbol=NULL; /* for error message */
	    ;
    break;}
case 134:
#line 1301 "user.y"
{
		defined_symbol = *new_symbol(yyvsp[0].string,&symbol_set[Pair]);
		current_class = defined_symbol->data->class.surface;
		class_flags = SURFACE_FLAG | LEXICAL_FLAG;
	    ;
    break;}
case 135:
#line 1309 "user.y"
{
	    ;
    break;}
case 136:
#line 1312 "user.y"
{
	    ;
    break;}
case 137:
#line 1317 "user.y"
{
	  s_symbol *class_symbol;

	  class_symbol = *find_symbol(yyvsp[0].string,&symbol_set[Pair]);
	  switch (class_symbol->kind) {
	      case Surface_Letter:
		      class_flags &= SURFACE_FLAG;
		      set_bit(current_class,
			      (long) *(class_symbol->data->pair.surface));
		      break;
	      case Lexical_Letter:
		      class_flags &= LEXICAL_FLAG;
	      /*FALLTHROUGH*/
	      case Pair_Letter:
		      set_bit(current_class,
			      (long) *(class_symbol->data->pair.lexical));
		      break;
	      case Surface_Class:
		      class_flags &= SURFACE_FLAG;
		      assign_or(current_class,
				class_symbol->data->class.surface);
		      break;
	      case Lexical_Class:
		      class_flags &= LEXICAL_FLAG;
	      /*FALLTHROUGH*/
	      case Pair_Class:
		      if (class_symbol == defined_symbol)
			fatal_error(
			"symbol class is used in its own definition");
		      assign_or(current_class,
				class_symbol->data->class.lexical);
		      break;
	      default:
		      fatal_error("program bug: no such class kind");
		      break;	
	  }
	  if (class_flags == 0)
		fatal_error("you cannot mix %s (class=\"%s\", symbol=\"%s\")",
			    "pure lexical and surface symbols in same class",
			    defined_symbol->name,
			    class_symbol->name);
      ;
    break;}
case 138:
#line 1360 "user.y"
{ /* allow special characters to be used as a class definition */
	/* TODO: cleanup the cases where this name could be printed ?? */
	s_symbol *class_symbol;

	class_symbol= *find_symbol(yyvsp[0].string,&symbol_set[Pair]);
	switch (class_symbol->kind) {
	    case Surface_Letter:
		    class_flags &= SURFACE_FLAG;
		    set_bit(current_class,
			    (long) *(class_symbol->data->pair.surface));
		    break;
	    case Lexical_Letter:
		    class_flags &= LEXICAL_FLAG;
	    /*FALLTHROUGH*/
	    case Pair_Letter:
		    set_bit(current_class,
			    (long) *(class_symbol->data->pair.lexical));
		    break;
	    default:
		    fatal_error("program bug: no such class kind");
		    break;	
	}
	if (class_flags == 0)
	      fatal_error("%s %s (class=\"%s\", symbol=\"%s\")",
			  "you cannot mix pure lexical and surface",
			  "symbols in the same class",
			  defined_symbol->name,
			  class_symbol->name);
      ;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 498 "/usr/share/bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;
}
#line 1390 "user.y"
