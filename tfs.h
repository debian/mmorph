/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
*/
#ifndef tfs_h
#define tfs_h

extern int  add_tfs();
extern void open_tfs_file();
extern void tfs_table_init();
extern void tfs_table_write();
extern void dump_tfs_table();
extern s_tfs *tfs_table;

#endif	/* tfs_h */
