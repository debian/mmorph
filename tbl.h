/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
*/

#ifndef tbl_h
#define tbl_h

#define NUL_CHAR	'\0'
#define FIELD_SEP	'\t'
#define SUBFIELD_SEP	'|'
#define SUBSUBFIELD_SEP	'\\'
#define RECORD_SEP	'\n'
/* UNKNOWN_MORPH should contain the SUBSUBFIELD_SEP */
#define UNKNOWN_MORPH	"??\\??"

/* ordinal number of specific fields, numbering from 0 */
#define form_field	2
#define class_field	1
#define morph_field	3

extern void lookup_tbl();
extern t_boolean fold_case();

#endif	/* tbl_h */
