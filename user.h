/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
*/
#ifndef user_h
#define user_h
#include "config.h"
#if defined(STDC_HEADERS) && ! MALLOC_FUNC_CHECK
#include <stdlib.h>	/* ANSI & POSIX types */
#endif
#include <stdio.h>
#include <errno.h>


/* because they may not be declared in errno.h: */
extern int  sys_nerr;
extern char *sys_errlist[];

#ifndef FALSE
#define FALSE   (0)
#endif

#ifndef TRUE
#define TRUE    (1)
#endif

#define DEBUG   1

/* debugging options */
#define	    DEBUG_INIT	    (1L << 0)
#define	    DEBUG_YACC	    (1L << 1)
#define	    DEBUG_COMBINE   (1L << 2)
#define	    DEBUG_SPELL     (1L << 3)
#define	    DEBUG_STAT      (1L << 4)
#define	    DEBUG_ALL	    (~ 0L)

/* grammar tracing levels */
#define	    TRACE_GOAL	    1
#define	    TRACE_RULE	    2
#define	    TRACE_FAIL	    10

/* spelling tracing levels */
#define	    TRACE_VALID_SURFACE	    1
#define	    TRACE_LEXICAL_MATCH	    2
#define	    TRACE_LEFT_MATCH	    3
#define	    TRACE_BLOCKING	    4
#define	    TRACE_NON_BLOCKING	    5

#ifdef MALLOC_FUNC_CHECK
#define	    EXIT(val) { malloc_shutdown();  exit(val); }
#else
#define	    EXIT(val) exit(val)
#endif

#ifndef MAX
#define MAX(a,b)    (((a) > (b))?(a):(b))
#endif
#ifndef MIN
#define MIN(a,b)    (((a) < (b))?(a):(b))
#endif

#define t_str char *
#define t_ptr char *

typedef int t_boolean;

typedef short t_card;

#define T_CARD_MAX MAXSHORT
#define T_CARD_TO_STRING(x, str) \
    *(unsigned char *)(str) = (unsigned char) ((int)(x) & UCHAR_MAX), \
    *((unsigned char *)(str) + 1) = (unsigned char) (((int)(x) >> CHAR_BIT) & UCHAR_MAX)

#define STRING_TO_T_CARD(str, x) \
    (x) = (t_card) *((unsigned char *) (str)), \
    (x) |= (t_card) ((int) *((unsigned char *)(str) + 1) << CHAR_BIT)

typedef short t_index;
typedef unsigned char t_letter;
typedef unsigned long t_flag;
typedef unsigned long t_segment_id;

#define NO_INDEX    ((short) -1)

#include "version.h"
#include "bitmap.h"
#include "chain.h"
#include "unify.h"
#include "symbols.h"
#include "spell.h"
#include "mymalloc.h"
#include "output.h"
#include "combine.h"
#include "database.h"
#include "tfs.h"
#include "tbl.h"
#include "crc32file.h"

extern unsigned long debug;
extern int  trace_level;
extern int  spell_trace_level;
extern t_boolean want_flush;
extern t_boolean want_segment_id;
extern t_boolean mark_unknown;
extern t_boolean extend_morph_field;
extern t_boolean overwrite_morph_field;
extern t_boolean fold_case_always;
extern t_boolean fold_case_fallback;
extern char *begin_sentence_class;

/* declarations to shutup "gcc -Wall", and lint */
#ifdef UNDEF
#ifndef STDC_HEADERS
extern int  getopt();
extern int  fclose();
extern int  fread();
extern int  fwrite();
extern int  isatty();

#endif
#endif

#define DIV_ROUND_UP(a,b) (((int)(a)+(int)(b)-1)/(int)(b))

#endif	/* user_h */
