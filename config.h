/* config.h.  Generated automatically by configure.  */
/* config.h.in.  Generated automatically from configure.in by autoheader.  */
#ifndef config_h
#define config_h

/* Define to empty if the keyword does not work.  */
/* #undef const */

/* Define to `long' if <sys/types.h> doesn't define.  */
/* #undef off_t */

/* Define to `unsigned' if <sys/types.h> doesn't define.  */
/* #undef size_t */

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1

/* The number of bytes in a long.  */
#define SIZEOF_LONG 4

/* Define if you have the <limits.h> header file.  */
#define HAVE_LIMITS_H 1

/* Define if you have the <memory.h> header file.  */
#define HAVE_MEMORY_H 1

/* Define if you have the <string.h> header file.  */
#define HAVE_STRING_H 1

/* Define if you have the <sys/file.h> header file.  */
#define HAVE_SYS_FILE_H 1

/* Define if you have the <unistd.h> header file.  */
#define HAVE_UNISTD_H 1
/* this file will be appended to the bottom of config.h */
#ifdef HAVE_SYS_FILE_H
#  include <sys/file.h>
#endif
#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif

/* TODO: An ANSI string.h and pre-ANSI memory.h might conflict.  */
#if defined (HAVE_STRING_H)
# include <string.h>
#else
# include <strings.h>
#endif

#if defined (HAVE_MEMORY_H)
#  include <memory.h>
#else
#  define memcpy(D, S, N) bcopy((S), (D), (N))
#endif

#endif /* ! config_h */
