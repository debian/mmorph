/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
*/
#ifndef chain_h
#define chain_h

typedef struct chain_s {
    char       *item;
    struct chain_s *next;
}           s_chain;

extern s_chain *insert_chain_link();
extern s_chain *reverse_chain();
extern void map_chain();
extern void free_chain();

#endif	/* chain_h */
