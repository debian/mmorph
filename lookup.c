/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
*/
/*
    lookup.c

    quick hack to lookup words in the database
    should be changed to use SGML API
*/

#include <ctype.h>
#include "user.h"


#define MAXSTRLNG 128

/*
    Read one word per line, and look it up.
    All characters count except newline.
 */
static      t_boolean
get_segment(infile, segment_id, segment, tfs)
FILE       *infile;
t_segment_id *segment_id;	/* output */
char      **segment;	/* output */
s_tfs     **tfs;	/* output */

{

    static char string_segment[MAXSTRLNG];
    static t_segment_id id_count = 0;	/* 32 bits = 4294967296 ids */
    char       *s;
    int         c;

    /*
       read a word, i.e all the characters on the line fgets is not used
       because it takes the newline in.
    */
    s = string_segment;
    while ((c = getc(infile)) != EOF
	   && (s - string_segment < MAXSTRLNG - 1)
	   && (c != (int) '\n')) {
	*s++ = (char) c;
    }
    *s = '\0';
    *segment_id = id_count++;
    *segment = string_segment;
    *tfs = NULL;	/* no yet implemented */
    if (c == (int) '\n')
	return (TRUE);
    else if (c == EOF)
	if (s == string_segment)
	    return (FALSE);	/* normal EOF */
	else {
	    print_warning("input file does not terminate %s",
			  "with a newline");
	    return (TRUE);
	}
    else
	fatal_error("word too long (max %d):\n %s",
		    MAXSTRLNG - 1, string_segment);
    return (FALSE);	/* shut up gcc -Wall */
    /* NOTREACHED */
}

/* see print_projection()
put_lex(segment_id,record, surface_lex)
{
}
*/

static void
reject_segment(segment_id, segment)
t_segment_id segment_id;	/* output */
char       *segment;	/* output */

{
    if (want_segment_id)
	print(rejectfile, "%d\t", segment_id);
    print(rejectfile, "%s\n", segment);
}

void
lookup(infile)
FILE       *infile;

{
    t_segment_id segment_id;
    char       *segment;
    s_tfs      *tfs;
    t_letter    surface_lex[MAXSTRLNG];
    t_boolean   found;
    t_boolean   folded;
    t_boolean   prompt_user;

    /* read a word and look-it up */
    prompt_user = isatty(fileno(infile)) && isatty(fileno(outfile));
    if (prompt_user) {
	print_out("%s", prompt);
	if (want_flush)	/* normally not necessary for ttys; just in case */
	    flush_out();
    }
    while (get_segment(infile, &segment_id, &segment, &tfs)) {
	if (fold_case_always) {
	    folded = fold_case((unsigned char *) segment,
			       (unsigned char *) surface_lex);
	    found = (map_letter((char *) surface_lex, surface_lex,
				Surface_Letter)
		     && db_forms_lookup(segment_id, surface_lex, tfs));
	}
	else {
	    folded = FALSE;
	    found = (map_letter(segment, surface_lex, Surface_Letter)
		     && db_forms_lookup(segment_id, surface_lex, tfs));
	}
	if (!fold_case_always && fold_case_fallback && !found) {
	    folded = fold_case((unsigned char *) segment,
			       (unsigned char *) surface_lex);
	    if (folded)
		found |= (map_letter((char *) surface_lex, surface_lex,
				     Surface_Letter)
			  && db_forms_lookup(segment_id, surface_lex, tfs));
	}
	if (!found) {
	    reject_segment(segment_id, segment);
	    if (want_segment_id)
		print_out("%d\t\n", segment_id);
	    else
		print_out("\n");
	}
	else if (!want_segment_id)
	    print_out("\n");	/* separator between possible analyses */
	if (prompt_user)
	    print_out("%s", prompt);
	if (want_flush)
	    flush_out();
    }
    if (prompt_user)
	print_out("\n");
}
