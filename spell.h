/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
*/
#ifndef spell_h
#define spell_h

#define LETTER_MAX	    ((t_letter) ((1L << BITS(t_letter)) -1))
#define LETTER_ANY	    LETTER_MAX
#define LETTER_MAP_SIZE	    (1L << BITS(char))
#define NUL_LETTER	    ((t_letter) 0)

#define SURFACE_FLAG	(1L)
#define LEXICAL_FLAG	(1L << 1)

/* if all lexical focus width are 1, then the stack size is equal to
   the lexical word length; so a good rule of the thumb is:
*/
#define POS_STACK_SIZE	CONCAT_SIZE

typedef struct {
    int         lexical_pos;
    int         surface_pos;
    int         spell_index;
}           s_pos_stack;

extern t_letter *surface_letter_map;
extern t_letter *lexical_letter_map;
extern unsigned char *letter_to_char_map;
extern t_letter letter_any[];
extern t_letter empty_string[];
extern t_letter *morpheme_boundary;
extern t_letter *word_boundary;
extern s_pair *morpheme_boundary_pair;
extern s_pair *word_boundary_pair;
extern t_letter first_lexical_letter;

extern t_letter *current_letter_map;
extern e_symbol_kind current_letter_kind;
extern int  alphabet_size;
extern int  lexical_alphabet_size;
extern t_card max_spell_length;
extern t_card max_left_context_length;
extern t_card max_rest_length;

extern void add_pair();
extern void add_explicit_pair();
extern t_boolean map_letter();
extern t_letter *find_letter();
extern s_pair *new_pair();
extern int  lexical_width();
extern void fill_applicability_map();
extern void prepare_spell();
extern void do_spell();
extern void is_pair_concrete();

#endif	/* spell_h */
