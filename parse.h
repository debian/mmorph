/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
*/
#ifndef parse_h
#define parse_h

#ifndef t_str
#define t_str char *
#endif

#define MAXSTRLNG 128

extern char *augment_file_name;
extern char *db_file_name;
extern char *gram_file_name;
extern int  parsing;
extern char *yytext;
extern int  yydebug;
extern int  yylineno;
extern int  linepos;
extern char *file_name_stack[];
extern short stack_index;
extern int  yychar;
extern int  augmenting;
extern int  allow_string_letters;
extern s_symbol *defined_symbol;
extern crc32file crc;
extern unsigned char crc_hash[];
extern t_boolean parse_only;
extern t_boolean normalize;
extern t_boolean normalize_flag;
extern int  yyparse();
extern void yyerror();
extern int  yylex();
extern void initfile();

#endif	/* parse_h */
