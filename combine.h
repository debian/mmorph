/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
*/
#ifndef combine_h
#define combine_h

extern s_bitmap *rule_maps[];
extern int  rule_instance_card;
extern s_rule_instance **rule_instance_ref;

extern void fill_rule_maps();
extern void combine_all();

#endif	/* combine_h */
