/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
*/
/*
    main.c

*/

#include <locale.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "user.h"
#include "parse.h"
#include "y.tab.h"
#include "tokenstr.h"

static char rcsid[]
= "@(#)main.c,v 2.21 1995/11/01 11:47:43 petitp Exp";


extern char *optarg;
extern int  optind;
extern int  opterr;
extern void lookup();

static char *version_string = VERSION_STR;
static char *patch_version_string = PATCH_VERSION_STR;
char       *db_file_name;
char       *gram_file_name;
char       *augment_file_name;
int         trace_level = 0;
int         spell_trace_level = 0;
int         parsing = FALSE;
crc32file   crc;
unsigned char crc_hash[CRC32FILE_HASHLEN];

unsigned long debug = 0L;
static char *progname;
static char *fullprogname;

static char *db_suffix = ".db";
static char *tfs_suffix = ".tfs";
static FILE *infile;
t_boolean   want_segment_id;
t_boolean   want_flush;
t_boolean   parse_only;
t_boolean   normalize;
t_boolean   normalize_flag;
t_boolean   mark_unknown;
t_boolean   fold_case_always;
t_boolean   fold_case_fallback;
t_boolean   extend_morph_field;
t_boolean   overwrite_morph_field;
char       *begin_sentence_class;

void
yyerror(s)
char       *s;
{
    fatal_error("%s; lookahead is \"%s\" (token %s %d)",
		s, yytext,
		((yychar < FIRSTTOKEN || yychar > LASTTOKEN)
		 ? "code" : token[yychar - FIRSTTOKEN]),
		yychar
	);
}

static void
init()
{
    init_symbol();
    /* setlocale is useful for isprint() in output.c and fold_case() */
    (void) setlocale(LC_CTYPE, "");
/*
    if (setlocale(LC_CTYPE, "") == NULL)
	TODO: do something, locale will be probably default "C"
*/
}

static void
print_usage()
{
    print_log("Usage for %s:\n",
	      fullprogname);
    print_log("info:\t %s%s\n",
	      progname,
	      " [-vh]");
    print_log("parse:\t %s%s%s\n",
	      progname,
	      " -y | -z [-a addfile]\n",
	  "\t\t-m morphfile [-d debugmap] [-l logfile] [infile [outfile]]");
    print_log("gen:\t %s%s%s\n",
	      progname,
	      " -n | -c [-t gram_trace] [-s spell_trace] [-a addfile]\n",
	  "\t\t-m morphfile [-d debugmap] [-l logfile] [infile [outfile]]");
    print_log("lookup:\t %s%s%s\n",
	      progname,
	      " [-fi] [-b | -k] [-r rejectfile]\n",
	  "\t\t-m morphfile [-d debugmap] [-l logfile] [infile [outfile]]");
    print_log("r/f:\t %s%s%s\n",
	      progname,
	  " -C lookup_classes [-fU] [-E | -O] [-b | [-k] [-B bos_class]]\n",
	  "\t\t-m morphfile [-d debugmap] [-l logfile] [infile [outfile]]");
    print_log("dump:\t %s%s%s\n",
	      progname,
	      " -p | -q\n",
	  "\t\t-m morphfile [-d debugmap] [-l logfile] [infile [outfile]]");

    print_log("\n%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
	      "  -a addfile\n\tadd morphological entries from addfile\n",
	      "  -B bos_class\n\tbeginning of sentence class: which precedes the first word of a sentence\n",
	      "  -b\tfold case before lookup\n",
	      "  -C lookup_classes\n\tlist of lookup classes (any suitable separator will do)\n",
	      "  -c\tcreate full forms database\n",
	      "  -d debugmap\n\tprint debug messages\n",
	      "\tdebugmap is composed of the following bits:\n",
	      "\t0x1  debug initialisation\n",
	      "\t0x2  debug yacc parsing\n",
	      "\t0x4  debug rule combination\n",
	      "\t0x8  debug spelling application\n",
	      "\t0x10 print statistics with -p or -q options\n",
	      "  -E\textend existing morphological annotation\n",
	      "  -O\toverwrite existing morphological annotation\n",
	      "  -f\tflush output after each word lookup\n",
	      "  -h\tprint help and exit\n",
	      "  -i\tprint input segment sequential number before each lookup result\n",
	      "  -k\tfold case fallback\n",
	      "  -l logfile\n\tfile for trace and error messages\n",
	   "  -m morphfile\n\tfile containing the morphology description\n",
	      "  -n\tno database (test mode)\n",
	      "  -p\tdump tfs database\n",
	      "  -q\tdump forms database\n",
	      "  -r rejectfile\n\tfile for rejected words\n",
	      "");
    print_log("%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
	      "  -s trace_level\n\ttrace spelling rule application\n",
	      "\ttrace_level can be (a level imply all preceding ones):\n",
	      "\t1  trace valid surface forms\n",
	      "\t2  trace rules whose lexical part match\n",
      "\t3  trace surface left context match (surface word construction)\n",
	    "\t4  trace surface right context mismatch and rule blocking\n",
	      "\t5  trace rule non blocking\n",
	      "  -t trace_level\n\ttrace grammar rule application\n",
	      "\ttrace_level can be (a level imply all preceding ones):\n",
	      "\t1  trace goal rules that apply\n",
	      "\t2  trace all rules that apply\n",
	      "\t10 trace rules that are tried but did not apply\n",
	      "  -U\tmark unknown words\n",
	      "  -v\tprint version and exit\n",
	      "  -y\tparse only\n",
	      "  -z\tparse and output normalized form\n",
	      "  infile\ttext input file, one word per line\n",
	      "  outfile\tambiguously tagged text output\n",
	      "");
}

void
main(argc, argv)
int         argc;
char       *argv[];

{
    int         c;
    int         rval;
    int         name_length;
    t_boolean   usage_error;
    t_boolean   nflag;
    t_boolean   cflag;
    t_boolean   dump_db;
    struct stat file_stat;
    char       *lookup_classes;

    extern int  optind;
    extern char *optarg;

    fullprogname = argv[0];
    progname = strrchr(fullprogname, (int) '/');
    if (progname == NULL)
	progname = fullprogname;
    else
	progname++;
    outfile = stdout;	/* fatal_error needs it */
    logfile = stderr;	/* fatal_error needs it */
    rejectfile = stderr;
    gram_file_name = NULL;
    augment_file_name = NULL;
    db_operation = Lookup;	/* default operation */
    usage_error = FALSE;
    nflag = FALSE;
    cflag = FALSE;
    want_segment_id = FALSE;
    want_flush = FALSE;
    parse_only = FALSE;
    normalize = FALSE;
    normalize_flag = FALSE;
    mark_unknown = FALSE;
    fold_case_always = FALSE;
    fold_case_fallback = FALSE;
    lookup_classes = NULL;
    begin_sentence_class = "\n";	/* cannot occur */
    extend_morph_field = FALSE;
    overwrite_morph_field = FALSE;
    dump_db = 0;
    while ((c = getopt(argc, argv, "a:bcd:fhikl:m:npqr:s:t:vyzB:C:EOU")) != -1) {
	switch (c) {
	    case 'd':
		if (sscanf(optarg, "%li", &debug) != 1) {
		    usage_error = TRUE;
		    print_log("error: bad debugmap: %s", optarg);
		}
		else {
		    if (debug & DEBUG_YACC)
			yydebug = 1;
		}
		break;
	    case 't':
		if (sscanf(optarg, "%d", &trace_level) != 1) {
		    usage_error = TRUE;
		    print_log("error: bad trace level: %s", optarg);
		}
		break;
	    case 's':
		if (sscanf(optarg, "%d", &spell_trace_level) != 1) {
		    usage_error = TRUE;
		    print_log("error: bad trace level: %s", optarg);
		}
		break;
	    case 'v':
		print_out("%s version %s\n%s\n", progname,
			  patch_version_string,
			  rcsid);
		EXIT(0);
		/* to shut up lint: */
		/* FALLTHROUGH */
	    case 'h':
		print_out("%s version %s\n", progname,
			  patch_version_string);
		print_usage();
		EXIT(0);
		/* to shut up lint: */
		/* FALLTHROUGH */
	    case 'l':
		if ((logfile = fopen(optarg, "w")) == NULL) {
		    logfile = stderr;
		    fatal_error("cannot open log file: %s", optarg);
		}
		break;
	    case 'r':
		if ((rejectfile = fopen(optarg, "w")) == NULL) {
		    fatal_error("cannot open reject file: %s", optarg);
		}
		break;
	    case 'm':
		gram_file_name = optarg;
		break;
	    case 'c':
		cflag = TRUE;
		if (nflag)
		    usage_error = TRUE;
		else
		    db_operation |= Create;	/* create new database */
		break;
	    case 'n':
		nflag = TRUE;
		if (cflag)
		    usage_error = TRUE;
		else
		    db_operation &= ~(Create | Lookup);	/* no database */
		break;
	    case 'a':
		db_operation |= Update;	/* augment */
		augment_file_name = optarg;
		break;
	    case 'i':
		want_segment_id = TRUE;
		break;
	    case 'b':	/* don't bother checking -b/-k/-B incompatibilities */
		fold_case_always = TRUE;
		fold_case_fallback = FALSE;
		break;
	    case 'k':
		fold_case_fallback = TRUE;
		fold_case_always = FALSE;
		break;
	    case 'f':
		want_flush = TRUE;
		break;
	    case 'p':
		dump_db = 1;
		break;
	    case 'q':
		dump_db = 2;
		break;
	    case 'z':
		normalize_flag = TRUE;
		/* FALLTHROUGH */
	    case 'y':
		parse_only = TRUE;
		db_operation &= ~(Create | Lookup);
		break;
	    case 'U':
		mark_unknown = TRUE;
		break;
	    case 'E':
		extend_morph_field = TRUE;
		overwrite_morph_field = FALSE;
		break;
	    case 'O':
		overwrite_morph_field = TRUE;
		extend_morph_field = FALSE;
		break;
	    case 'C':
		lookup_classes = optarg;
		break;
	    case 'B':
		begin_sentence_class = optarg;
		break;
	    default:
		usage_error = TRUE;
		break;
	}
    }
    if (usage_error) {
	print_usage();
	EXIT(2);
    }
    if (gram_file_name == NULL) {
	print_usage();
	fatal_error("error: no grammar file provided");
    }
    initfile(gram_file_name);
    if (!(db_operation & Lookup)) {
	if (spell_trace_level < 1)
	    spell_trace_level = 1;	/* don't be completely silent! */
    }
    else {	/* make the database name from the description file name */
	name_length = strlen(gram_file_name);
	MY_STRALLOC(db_file_name,
	name_length + MAX((int) strlen(db_suffix), (int) strlen(tfs_suffix))
		    + 1);
	(void) strcpy(db_file_name, gram_file_name);
	(void) strcpy(db_file_name + name_length, db_suffix);
	if (!(db_operation & Create)) {
	    if (stat(gram_file_name, &file_stat) < 0)
		fatal_error("cannot access file \"%s\" (errno=%d=%s)",
			    gram_file_name, errno,
			    (errno < sys_nerr ? sys_errlist[errno] : "?"));
#ifdef COMMENTED_OUT
	    time_t      gram_file_mtime;

	    /* this code is replaced by the crc checking */
	    gram_file_mtime = file_stat.st_mtime;
	    if (stat(db_file_name, &file_stat) < 0)
		fatal_error("cannot access file \"%s\" (errno=%d=%s)",
			    db_file_name, errno,
			    (errno < sys_nerr ? sys_errlist[errno] : "?"));
	    if (gram_file_mtime > file_stat.st_mtime)
		fatal_error("database file \"%s\" %s%s\"%s\": %s",
			    db_file_name,
			    "is out of date with respect to\n",
			    "the morphology description in",
			    gram_file_name,
			    "please recreate it");
#endif
	}
	db_forms_init(db_file_name, db_operation);
	(void) strcpy(db_file_name + name_length, tfs_suffix);
	open_tfs_file(db_file_name, db_operation);
    }
    if (optind >= argc) {
	infile = stdin;
    }
    else {
	if (!(db_operation & Lookup))
	    print_warning("input file \"%s\" will be ignored",
			  argv[optind]);
	if ((infile = fopen(argv[optind], "r")) == NULL)
	    fatal_error("cannot open input file: %s", argv[optind]);
	optind++;
    }
    if (optind >= argc) {
	outfile = stdout;
    }
    else {
	if ((outfile = fopen(argv[optind], "w")) == NULL)
	    fatal_error("cannot open output file: %s", argv[optind]);
	optind++;
    }
    init();
    parsing = TRUE;
    crc32file_clear(&crc);
    rval = yyparse();
    parsing = FALSE;	/* parse complete, error message format change */
    if (debug & DEBUG_INIT)
	print_log("parse = %d\n", rval);
    if (rval != 0)
	fatal_error("parsing aborted (code %d)", rval);
    if (db_operation & Lookup) {
	if (db_operation & (Create | Update)) {
	    db_forms_complete();	/* save what's done */
	    tfs_table_write(db_operation, db_file_name, crc_hash);
	}
	if (dump_db == 1)
	    dump_tfs_table();	/* dump on outfile, stat on logfile */
	else if (dump_db == 2)
	    db_forms_dump();	/* dump on outfile, stat on logfile */
	else if (lookup_classes)
	    lookup_tbl(infile, lookup_classes);
	else
	    lookup(infile);
	db_forms_close();
    }
    EXIT(0);
    /* NOTREACHED */
}
