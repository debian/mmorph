/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
*/
/*
    handle all printing
*/

#include <varargs.h>
#include <ctype.h>
#include "user.h"
#include "output.h"
#include "parse.h"


FILE       *outfile;
FILE       *logfile;
FILE       *rejectfile;
char       *prompt = PROMPT;


/*VARARGS*/
void
fatal_error(va_alist)
va_dcl

{
    va_list     ap;
    char       *format;

    va_start(ap);
    format = va_arg(ap, char *);
    (void) fflush(outfile);
    if (parsing) {
	if (fprintf(logfile,
		    "\"%s\", line %d: syntax error before character %d\n",
		    file_name_stack[stack_index], yylineno, linepos)
	    == EOF)
	    EXIT(3);
	if (defined_symbol != NULL)
	    if (fprintf(logfile, "%s %s \"%s\"\n",
			"error in the definition of",
			symbol_kind_str[defined_kind],
			defined_symbol->name) == EOF)
		EXIT(3);
    }
    if (vfprintf(logfile, format, ap) == EOF)
	EXIT(3);
    if (fprintf(logfile, "\n") == EOF)
	EXIT(3);
    va_end(ap);
    EXIT(2);
}

/*VARARGS*/
void
print_warning(va_alist)
va_dcl

{
    va_list     ap;
    char       *format;

    va_start(ap);
    format = va_arg(ap, char *);
    (void) fflush(outfile);
    if (fprintf(logfile, "Warning: ") == EOF)
	EXIT(3);
    if (vfprintf(logfile, format, ap) == EOF)
	EXIT(3);
    if (fprintf(logfile, "\n") == EOF)
	EXIT(3);
    va_end(ap);
}

/*VARARGS*/
void
print_out(va_alist)
va_dcl

{
    va_list     ap;
    char       *format;

    va_start(ap);
    format = va_arg(ap, char *);
    if (vfprintf(outfile, format, ap) == EOF)
	EXIT(3);
    va_end(ap);
}

void
flush_out()
{
    (void) fflush(outfile);
}

/*VARARGS*/
void
print_log(va_alist)
va_dcl

{
    va_list     ap;
    char       *format;

    va_start(ap);
    format = va_arg(ap, char *);
    if (vfprintf(logfile, format, ap) == EOF)
	EXIT(3);
    va_end(ap);
}

/*VARARGS*/
void
print(va_alist)
va_dcl

{
    va_list     ap;
    char       *format;
    FILE       *file;

    va_start(ap);
    file = va_arg(ap, FILE *);
    format = va_arg(ap, char *);
    if (vfprintf(file, format, ap) == EOF)
	EXIT(3);
    va_end(ap);
}

void
print_letter(file, letter)
FILE       *file;
t_letter    letter;

{
    int         status;
    int         c;

    if (letter == LETTER_ANY) {
	status = putc('?', file);
    }
    else {
	if ((c = (int) letter_to_char_map[letter]) > 0) {
	    switch (c) {	/* reverse of what is in tokenize.l */
		case '\n':
		    status = fputs("\\n", file);
		    break;
		case '\t':
		    status = fputs("\\t", file);
		    break;
		default:
		    if (isprint(c))
			status = putc(c, file);
		    else
			status = fprintf(file, "\\0%3o", c);
		    break;
	    }
	}
	else if (letter == *morpheme_boundary)
	    status = putc('+', file);
	else if (letter == *word_boundary)
	    status = putc('~', file);
	else {
	    status = putc('&', file);
	    if (status != EOF)
		status = fputs(symbol_set[Pair].ref[letter]->name,
			       file);
	    if (status != EOF)
		status = putc(';', file);
	}
    }
    if (status == EOF)
	if (file != logfile)
	    fatal_error("error while writing in a file (errno=%d=%s)",
		      errno, (errno < sys_nerr ? sys_errlist[errno] : "?"));
	else	/* cannot print error message to log */
	    EXIT(3);
}

void
print_string_l(file, string, string_length)
FILE       *file;
t_letter   *string;
int         string_length;

{
    t_letter   *letter;

    letter = string;
    while (letter < string + string_length) {
	print_letter(file, *letter);
	letter++;
    }
}

/*
    Same as before print_string_l(),
    but for NUL terminated letter strings
*/
void
print_string(file, string)
FILE       *file;
t_letter   *string;

{
    t_letter   *letter;

    letter = string;
    while (*letter) {
	print_letter(file, *letter);
	letter++;
    }
}
