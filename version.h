/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
    Modificado por la RAE.
*/
#ifndef version_h
#define version_h

#define CREATOR_NAME	"mmorph"
#define MAJOR_VERSION	2
#define MINOR_VERSION	3
#define PATCH_LEVEL	4

#define VERSION_STR	"2.3"
#define PATCH_VERSION_STR "2.3.4_2"

#endif	/* version_h */
