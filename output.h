/*
    mmorph, MULTEXT morphology tool
    Version 2.3, October 1995
    Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
    Dominique Petitpierre, <petitp@divsun.unige.ch>
*/
#ifndef output_h
#define output_h

#include "config.h"

/* user prompt when input and output are ttys */
#define	PROMPT	"> "

extern FILE *outfile;
extern FILE *logfile;
extern FILE *rejectfile;
extern char *prompt;

extern void fatal_error( /* va_alist */ );
extern void print_warning( /* va_alist */ );
extern void flush_out();
extern void print_out( /* va_alist */ );
extern void print_log( /* va_alist */ );
extern void print( /* va_alist */ );
extern void print_string();
extern void print_string_l();
extern void print_letter();

#ifdef UNDEF
/* make "gcc -Wall" and lint happy */
#ifndef STDC_HEADERS
extern int  printf();
extern int  fprintf();
extern int  sscanf();
extern int  vfprintf();
extern int  fflush();
extern int  fwrite();
extern int  fputs();
extern void exit();

#endif
#endif

#endif	/* output_h */
