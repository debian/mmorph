.\" @(#)mmorph.1  October 1995 ISSCO;
.\" mmorph, MULTEXT morphology tool
.\" Version 2.3, October 1995
.\" Copyright (c) 1994,1995 ISSCO/SUISSETRA, Geneva, Switzerland
.\" Dominique Petitpierre, <petitp@divsun.unige.ch>
.TH MMORPH 5  "Version 2.3, October 1995"

.SH NAME
mmorph \- MULTEXT morphology tool formalism syntax

.SH DESCRIPTION
A
.B mmorph
morphology description file is divided into declaration sections.  Each
section starts by a section header (`\fB@\ Alphabets\fP',
`\fB@\ Attributes\fP', etc.)  followed by a sequence of declarations.  Each
declaration starts by a name, followed by a colon (`\fB:\fP') and the
definition associated to the name.  Here is a brief description of each
section:
.br
.SH "\s+2@ Alphabets"
In this section the lexical and surface alphabet are declared.  All symbols
forming each alphabet has to be listed.  Symbols may appear in both the
lexical and surface alphabet definition in which case it is considered a
bi-level symbol, otherwise it is a lexical only or surface only symbol.
Symbols are usually letters (eg.  \fBa, b, c\fP) , but may also consist of
longer names (\fBbeta\fP, \fBschwa\fP).  Symbol names consisting of one
special character (`\fB:\fP' or `\fB(\fP') may be specified by enclosing
them in double quotes (`\fB:\fP' or `\fB(\fP').
.br
Example:
.RS
.HP
.ft B
Lexical :
a b c d e f g h i j k l m n o p q r s t u 
v w x y z "\-" "." "," "?" "!" "\\"" "'" ":" ";" "(" ")"
strong_e
.HP
.ft B
Surface :
a b c d e f g h i j k l m n o p q r s t u 
v w x y z "\-" "." "," "?" "!" "\\"" "'" ":" ";" "(" ")" "\ "
.RE
.LP
In this example, the symbol \fBstrong_e\fP is lexical only, the symbol
\fB"\ "\fP (space) is surface only.  All the other symbols are bi-level.
.LP
All the strings appearing in the rest of the grammar will be made
exclusively of symbols declared in this section.
.SH "\s+2@ Attributes"
In this section, the name of attributes (sometimes called features) and
their associated value set.  At most 32 different values may be declared
for an attribute.
.br
Examples:
.RS
.ft B
.nf

Gender : feminine masculine neuter
Number : singular plural
Person : 1st 2nd 3rd
Transitive : yes no
Inflection : base intermediate final
.fi
.RE
.LP
In the current version of the implementation value sets of different
attributes are incompatible, even if they are defined identically.  To
overcome this restriction, in a future version this section will be split
into two:  declaration of value sets and declaration of attributes.
.RE
.SH "\s+2@ Types"
In this section, the different types of feature structures are declared.
The attributes allowed for each type are listed.  Attributes that are only
used within the scope of the tool and have no meaning outside can be listed
after a bar (`\fB|\fP').  The values of these local attributes ar not
stored in the database or written on the final output of the program.
.br
Examples:
.RS
.ft B
.nf

Noun : Gender Number
Verb : Tense Person Gender Number Transitive | Inflection

.fi
.RE
.SH "\s+2Typed feature structures"
Typed feature structures are used in the grammar and spelling rules.  It is
the specification of a type and the value of some associated attributes.
The list of attribute specifications is enclosed in square brackets
(`\fB[\fP' and `\fB]\fP').
.br
Example:
.RS
.nf
.ft B

Noun[ Gender=feminine Number=singular ]

.fi
.RE
.PP
It is possible to specify a set of values for an attribute by listing the
possible valuse separated with a bar (`\fB|\fP'), or the complement of a
set (with respect to all possible values of that attribute) indicated with
`\fB!=\fP' instead of `\fB=\fP'.
.br
Example:  Assuming the declaration of \fBGender\fP as above, the following two
typed feature structures are equivalent
.RS
.ft B
.nf

Noun[ Gender=masculine|neuter ]
Noun[ Gender!=feminine ]

.fi
.RE

.SH "\s+2@ Grammar"
This section contains the rules that specify the structure of words.  It
has the general shape of a context free grammar over typed feature
structures.  There are three basic types of rules:  binary, goal and affixes.
.PP
Binary rules specify the result of the concatenation of two
elements. This is written as:
.RS
.ft B
.nf

\fIRule_name\fP : \fILhs\fP <\- \fIRhs1 Rhs2\fP
.fi
.RE
.LP
where \fILhs\fP is called the left hand side, and \fIRhs1\fP and \fIRhs2\fP
the first and second part of the right hand side.  \fILhs\fP, \fIRhs1\fP
and \fIRhs2\fP are
specified as typed feature structures.
.br
Example:
.RS
.ft B
.nf

.ta 0.8iL 1.1iL
Rule_1	: Noun[ Gender=feminine Number=singular ]
	<\-	Noun[ Gender=feminine Number=singular ]
		NounSuffix[ Gender=feminine ]

.fi
.RE
.PP
Variables can be used to indicate that some attributes have the same value.
A variable is a name starting with a dollar (`\fB$\fP').
.br
Example:
.nf
.RS
.ft B
.nf

.ta 0.8iL 1.1iL
Rule_2	: Noun[ Gender=$A Number=$number ]
	<\-	Noun[ Gender=$A Number=$number ]
		NounSuffix[ Gender=$A ]

.fi
.RE
.PP
If needed, both a variable and a value specification can be given for an
attribute (only once per attribute):
.br
Example:
.RS
.ft B
.nf

.ta 0.8iL 1.1iL
Rule_3	: Noun[ Gender=$A Number=$number ]
	<\-	Noun[ Gender=$A Number=$number ]
		NounSuffix[ Gender=$A=masculine|neuter ]
.fi
.RE
.PP
Affix rules define basic elements of the concatenations specified by binary
rules (together with lexical entries, see the section \fB@\ Lexicon\fP below).
An affix rule consists of lexical string associated to a typed feature
structure.
.br
Examples:
.nf
.RS
.ft B
.nf

Plural_s : "s" NounSuffix[ Number=plural ]
Feminine_e : "e" NounSuffix[ Gender=feminine ]
ing : "ing" VerbSuffix[ Tense=present_participle ]
.fi
.RE
.PP
Goal rules specify the valid results constructed by the grammar.
They consist of just a typed feature structure.
.br
Examples:
.nf
.RS
.ft B
.nf

Goal_1	: Noun[]
Goal_2	: Verb[ inflection=final ]
.fi
.RE
.PP
In addition to these three basic rule types, there are prefix or suffix
composite rules and unary rules.
A unary rule consist of a left hand side and a right hand side.
.br
Example:
.RS
.ft B
.ta 0.8iL 1.1iL
.nf

Rule_4	: Noun[ gender=$G number=plural ]
	<\-	Noun[ gender=$G number=singular invariant=yes]
.fi
.RE
.PP
Prefix and suffix composite rules have the same shape as binary rules
except that one part of the right hand side is an affix (i.e. has an
associated string).
.br
Examples:
.nf
.RS
.ft B
.nf

.ta 0.8iL 1.1iL
Append_e	: Noun[ Gender=feminine Number=$number ]
	<\-	Noun[ Gender=feminine Number=$number ]
		"e" NounSuffix[ Gender=feminine ]

anti	: Noun[ Gender=$gender Number=$number ]
	<\-	"anti" NounPrefix[]
		Noun[ Gender=$gender Number=$number ]

.fi
.RE
.SH "\s+2@ Classes"
This optional section contains the definition of symbol classes. Each class is
defined as a set of symbols, or other classes. If the class contains
only bi-level elements it is a bi-level class, otherwise it is a lexical
or surface class.
.br
Examples:
.RS
.ft B
.nf

Dental : d t
Vowel : a e i o u
Vowel_y : Vowel y
Consonant: b c d f g h j k l m n p q r s t v w x z

.fi
.RE
.SH "\s+2@ Pairs"
This optional section contains the definition of pair disjunctions.  Each
disjunction is defined as a set of pairs.  Explicit pairs specify a
sequence of surface symbols and a sequence of zero or one lexical symbol,
one of them possibly empty.  A sequence is enclosed between angle brackets
`\fB<\fP' and `\fB>\fP'.  The empty sequence is indicated with `\fB<>\fP'.  In
the current implementation only the surface part of a pair can be a
sequence of more than one element.  The special symbol `\fB?\fP' stands for
the class of all possible symbols, including the morpheme and word
boundary.
.br
Examples:
.RS
.ft B
.nf

s_x_z_1 : s/s x/x z/z
VowelPair1: a/a e/e i/i o/o u/u 
VowelPair2: Vowel/Vowel
ie.y: <i e>/y
Delete_e: <>/e
Insert_d: d/<>
Surface_Vowel: Vowel/?
Lexical_s:  ?/s
.fi
.HP
.ft B
DoubleConsonant: <b\ b>/b <d\ d>/d <f\ f>/f <g\ g>/g <k\ k>/k <m\ m>/m
<p\ p>/p <s\ s>/s <t\ t>/t <v\ v>/v <z\ z>/z
.RE
.LP
Note that \fBVowelPair1\fP and \fBVowelPair2\fP don't specify the same thing:
\fBVowelPair2\fP would match \fBa/o\fP but \fBVowelPair1\fP would not.
.LP
Implicit pairs are specified by the name of a bi-level symbol or a bi-level
class.
.br
Examples:  the following \fBs_x_z_2\fP and \fBVowelPair3\fP are equivalent
to the above \fBs_x_z_1\fP and \fBVowelPair2\fP (assuming that \fBs\fP,
\fBx\fP, \fBz\fP and \fBVowel\fP are bi-level symbols and classes).
.RS
.ft B
.nf

s_x_z_2 : s x z
VowelPair3 : Vowel
.fi
.RE
.LP
In a pair disjunction all lexical parts should be disjoint. This means you 
cannot specify for the same pair disjunction \fBa/a\fP and \fBo/a\fP or
\fBa/a\fP and
\fBVowel/Vowel.\fP
.LP
In a future version this section will be split in two:  simple pair
disjunctions and pair sequences.
.SH "\s+2@ Spelling"
In this section are declared the two level spelling rules.  A spelling rule
consist of a kind indicator followed by a left context a focus and a right
context.  The kind indicator is `\fB=>\fP' if the rule is optional,
`\fB<=>\fP' if it is obligatory and `\fB<=\fP' if it is a surface coercion
rule.  The contexts may be empty.  The focus is surrounded by two
`\fB\-\fP'.  The contexts and the focus consist of a sequence of pairs or
pair disjunctions declared in the `\fB@\ Pairs\fP section.  A morpheme
boundary is indicated by a `\fB+\fP' or a `\fB*\fP', a word boundary is
indicated by a `\fB~\fP'.
.br
Examples:
.RS
.ft B
.nf

Sibilant_s: <=> s_x_z_1 * \- e/<> \- s
Gemination: <=>
	Consonant Vowel \- DoubleConsonant \- * Vowel
i_y_optionnel: => a \- i/y \- * ?/e
.fi
.RE
.LP
Constraints may be specified in the form of a list of typed feature
structures.  They are affix-driven:  the rule is licensed if at least one of
them subsumes the closest corresponding affix.  The morpheme boundary
indicated by a star (`\fB*\fP') will be used to determine which affix it
is.  If there is no such indication, then the affix adjacent to the
morpheme where the first character of the focus occurs is used.  In case
there is no affix, the typed feature structure of the lexical stem is used.
.br
Example:
.RS
.ft B
.nf

Sibilant_s: <=>
    s_x_z_1 * \- e/<> \- s NounSuffix[ Number=plural ]
.fi
.RE
.SH "\s+2@ Lexicon"
This section is optional and can also be repeated.  This section lists all
the lexical entries of the morphological description.  Unlike the other
sections, definitions do not have a name.  A definition consist of a typed
feature strucure followed by a list of lexical stems that share that
feature structure.  A lexical stem consists of the string used in the
concatenation specified by the grammar rules followed by `\fB=\fP' and a
reference string.  The reference string can be anything and usually is used
to indicate the canonical form of the word or an identifier of an external
database entry.
.br
Examples:
.nf
.RS
.ft B
.nf
Noun[ Number=singular ] "table" = "table" "chair" = "chair"
Verb[ Transitive=yes|no Inflection=base ] "bow" = "bow1"
Noun[ Number=singular ] "bow" = "bow2"
.fi
.RE
.LP
If the stem string and the reference strings are identical, only one needs
to be specified.
.br
Example:
.nf
.RS
.ft B
.nf

Noun[ Number=singular ] "table" "chair"
.fi
.RE
.SH FORMAL\ SYNTAX
The formal syntax description below is in Backus Naur Form (BNF).
The following conventions apply:
.nf
.ta 1.0iL

<\fIid>\fR	is a non-terminal symbol (within angle brackets).
\fIID\fP	is a token (terminal symbol, all uppercase).
<\fIid>?\fR	means zero or one occurrence of <\fIid>\fR (i.e. <\fIid>\fR is optional).
<\fIid>*\fR	is zero or more occurrences of <\fIid>\fR.
<\fIid>+\fR	is one or more occurrences of <\fIid>\fR.
::=	separates a non-terminal symbol and its expansion.
|	indicates an alternative expansion.
;	starts a comment (not part of the definition).

.fi
The start symbol corresponding to a complete description is named
\fB<Start>\fP.
Symbols that parse but do nothing are marked with
`;\ not\ operational'.
.nf

.ta 1.8iL 2.2iL
<Start>	::=	<AlphabetDecl> <AttDecl> <TypeDecl> <GramDecl>
	 	<ClassDecl>? <PairDecl>? <SpellDecl>? <LexDecl>*

<AlphabetDecl>	::=	ALPHABETS <LexicalDef> <SurfaceDef>

<LexicalDef>	::=	<LexicalName> COLON <LexicalSymbol>+
    
<SurfaceDef>	::=	<SurfaceName> COLON <SurfaceSymbol>+

<LexicalSymbol>	::=	<LexicalSymbolName>    ; lexical only
	|	<BiLevelSymbolName>    ; both lexical and surface 

<SurfaceSymbol>	::=	<SurfaceSymbolName>    ; surface only
	|	<BiLevelSymbolName>    ; both lexical and surface

<AttDecl>	::=	ATTRIBUTES <AttDef>+

<AttDef>	::=	<AttName> COLON <ValName>+

<TypeDecl>	::=	TYPES <TypeDef>+

<TypeDef>	::=	<TypeName> COLON <AttName>+ <NoProjAtt>?

<NoProjAtt>	::=	BAR <AttName>+

<LexDecl>	::=	LEXICON <LexDef>+

<LexDef>	::=	<Tfs> <Lexical>+

<Lexical>	::=	LEXICALSTRING <BaseForm>?

<BaseForm>	::=	EQUAL LEXICALSTRING 

<Tfs>	::=	<TypeName> <AttSpec>? 

<VarTfs>	::=	<TypeName> <VarAttSpec>? 

<AttSpec>	::=	LBRA <AttVal>* RBRA 

<VarAttSpec>	::=	LBRA <VarAttVal>* RBRA 

<AttVal>	::=	<AttName> <ValSpec> 

<VarAttVal>	::=	<AttName> <VarValSpec> 

<ValSpec>	::=	EQUAL <ValSet>
	|	NOTEQUAL <ValSet>

<VarValSpec>	::=	<ValSpec>
	|	EQUAL DOLLAR <VarName>
	|	EQUAL DOLLAR <VarName> <ValSpec>

<ValSet>	::=	<ValName> <ValSetRest>* 

<ValSetRest>	::=	BAR <ValName> 

<GramDecl>	::=	GRAMMAR <Rule>+

<RuleDef>	::=	<RuleName> COLON <RuleBody>

<RuleBody>	::=	<VarTfs> LARROW <Rhs>
	|	<Tfs>    \fR; goal rule\fP
	|	LEXICALSTRING <Tfs>    \fR; lexical affix\fP

<Rhs>	::=	<VarTfs>    \fR; unary rule\fP
	|	<VarTfs> <VarTfs>    \fR; binary rule\fP
	|	LEXICALSTRING <Tfs> <VarTfs>   \fR; prefix rule\fP
	|	<VarTfs> <Tfs> LEXICALSTRING    \fR; suffix rule\fP

<ClassDecl>	::=	CLASSES	<ClassDef>+

<ClassDef>	::=	<LexicalClassName> COLON <LexicalClass>+
	|	<SurfaceClassName> COLON <SurfaceClass>+
	|	<BiLevelClassName> COLON <BiLevelClass>+

<LexicalClass>	::=	<LexicalSymbol>
	|	<LexicalClassName>
	|	<BiLevelClassName>

<SurfaceClass>	::=	<SurfaceSymbol>
	|	<SurfaceClassName>
	|	<BiLevelClassName>

<BiLevelClass>	::=	<BiLevelSymbolName>
	|	<BiLevelClassName>

<PairDecl>	::=	PAIRS <PairDef>+

<PairDef>	::=	<PairName> COLON <PairDef>+

<PairDef>	::=	<PairName> COLON <Pair>+

<Pair>	::=	<SurfaceSequence> SLASH <LexicalSequence>
	|	<PairName>
	|	<BiLevelClassName>
	|	<BiLevelSymbolName>

SurfaceSequence	::=	LANGLE <SurfaceSymbol>* RANGLE
	|	SURFACESTRING
	|	<SurfaceClass>
	|	ANY

LexicalSequence	::=	LANGLE <LexicalSymbol>* RANGLE
	|	LEXICALSTRING
	|	<LexicalClass>
	|	ANY

<SpellDecl>	::=	SPELLING <SpellDef>+

<SpellDef>	::=	<SpellName> COLON <Arrow> <LeftContext> <Focus>
			    <RightContext> <Constraint>*

<LeftContext>	::=	<Pattern>*

<RightContext>	::=	<Pattern>*

<Focus>	::=	CONTEXTBOUNDARY <Pattern>+ CONTEXTBOUNDARY

<Pattern>	::=	<Pair>
	|	MORPHEMEBOUNDARY
	|	WORDBOUNDARY
	|	CONCATBOUNDARY

<Constraint>	::=	<Tfs>

<Arrow>	::=	RARROW 
	|	BIARROW 
	|	COERCEARROW

.ta 2.0iL 2.4iL
<AttName>	::=	NAME
<BiLevelClassName>	::=	NAME
<BiLevelSymbolName>	::=	NAME  | SYMBOLSTRING
<LexicalClassName>	::=	NAME
<LexicalName>	::=	NAME
<LexicalSymbolName>	::=	NAME  | SYMBOLSTRING
<PairName>	::=	NAME
<RuleName>	::=	NAME
<SpellName>	::=	NAME
<SurfaceClassName>	::=	NAME
<SurfaceName>	::=	NAME
<SurfaceSymbolName>	::=	NAME  | SYMBOLSTRING
<TypeName>	::=	NAME
<ValName>	::=	NAME
<VarName>	::=	NAME
.fi
.LP
.SS Simple tokens
Simple tokens of the BNF above are defined as follow:
The token name on the left correspond to the literal character
or characters on the right:
.nf

ANY	\fB?\fP
BAR	\fB|\fP
BIARROW	\fB<=>\fP
COERCEARROW	\fB<=\fP
COLON	\fB:\fP
CONCATBOUNDARY	\fB*\fP
CONTEXTBOUNDARY	\fB\-\fP
DOLLAR	\fB$\fP
EQUAL	\fB=\fP
LANGLE	\fB<\fP
LARROW	\fB<\-\fP
LBRA	\fB]\fP
MORPHEMEBOUNDARY	\fB+\fP
NOTEQUAL	\fB!=\fP
RARROW	\fB=>\fP
RANGLE	\fB<\fP
RBRA	\fB[\fP
SLASH	\fB/\fP
WORDBOUNDARY	\fB~\fP

ALPHABETS	\fB@Alphabets\fP
ATTRIBUTES	\fB@Attributes\fP
CLASSES	\fB@Classes\fP
GRAMMAR	\fB@Grammar\fP
LEXICON	\fB@Lexicon\fP
PAIRS	\fB@Pairs\fP
SPELLING	\fB@Spelling\fP
TYPES	\fB@Types\fP
.fi
.LP
In the section header tokens above, spaces may separate the `\fB\@\fP' from the
reserved word.
.LP
.SS Complex tokens
.TP
NAME
.br
is any sequence of letter, digit, underline (`\fB_\fP'), period (`\fB.\fP')
.br
Examples:
.nf
.RS
.ft B
category
33
Rule_9
__2__	
Proper.Noun
.RE
.ft R
.fi
.TP
LEXICALSTRING
.br
is a string of lexical symbols
.TP
SURFACESTRING
is a string of surface symbols
.TP
SYMBOLSTRING
.br
is a string of just just one character (used only in alphabet declaration).
.LP
A string consist of zero or more characters within double
quotes (`\fB"\fP').
Characters preceded by a backslash (`\fB\\\fP') are escaped
(the usual C escaping convention apply).
Symbols that have a name longer than one character are represented using
a SGML entity like notation: `\fB&\fIsymbolname\fB;\fR'.
The maximum number of symbols in a string is 127.
.br
Examples:
.nf
.RS
.ft B

"table"
","
""
"double quote is \\" and backslash is \\\\"
"&strong_e;"
"escape like in C : \\t is ASCII tab"
"escape with octal code: \\011 is ASCII tab"
.fi
.RE
.LP
Tokens can be separated by one or many blanks or comments.
.br
A blank separator is space, tab or newline.
.br
A comment starts with a semicolon and finishes at the next newline
(except when the semicolon occurs in a string.
.LP
Inclusion of files can be specified with the usual `\fB#include\fP' directive:
.br
Example:
.RS
.ft B
#include "verb.entries"
.RE
.LP
will splice in the content of the file \fBverb.entries\fP at the point
where this directive occurs.

The `\fB#\fP' should be the first character on the line.
Tabs or spaces may separate `\fB#\fP' and `\fBinclude\fP'.
The file name must be quoted.
Only tabs or spaces may occur on the rest of the line.
Inclusion can be nested up to 10 levels.
.SH SEE\ ALSO
.BR mmorph(1).
.HP
G. Russell and D. Petitpierre, \fIMMORPH \- The Multext
Morphology Program\fP, Version 2.3, October 1995, MULTEXT deliverable
report for task 2.3.1.
.SH AUTHOR
Dominique Petitpierre, ISSCO, <petitp@divsun.unige.ch>
.SH COMMENTS
The parser for the morphology description formalims above was written
using
.B yacc
(1) and
.B flex
(1).  Flex was written by Vern Paxson, <vern@ee.lbl.gov>, and is
distributed in the framework of the GNU project under the condition of the
GNU General Public License
